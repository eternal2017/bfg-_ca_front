export const adminSettingsStateSelector = state => state.adminSettings;

export const adminSettingsToShowSelector = state => adminSettingsStateSelector(state).selectedSettingsId;

export const departmentForEquipmentClassSelectOptionsSelector = state =>
  adminSettingsStateSelector(state).departmentForEquipmentClassSelectOptions;