import { SAVE_CURRENT_TASKS_DATA } from './actions';

import { deepPlainCopy } from '../../utils/deepPlainCopy';

/*
*
* Пока решено хранить задания только для одного уникального ключа одновременно, а если запрошены данные по другому
* ключу, то перезаписывать данные, чтобы не хранить много данных на клиенте, т.к. основной кейс использования пока -
* это выбор подразделения и класса РЦ один раз и дальнейшая работа с ними, а при смене сессии, данные
* автоматически обновляются.
* Если нужно будет хранить и предыдущие данные, то нужно будет просто добавить распакову ...state в возвращаемый
* объект
* */

export const tasks = (state = {}, action) => {
  switch(action.type){
  case SAVE_CURRENT_TASKS_DATA:
    const {
      tasksDataUniqId,
      tasksData
    } = action;
    return {
      [tasksDataUniqId]: deepPlainCopy(tasksData)
    };
  default:
    return state;
  }
};