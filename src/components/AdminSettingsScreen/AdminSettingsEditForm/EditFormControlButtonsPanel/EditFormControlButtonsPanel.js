import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

import { CANCEL_LABEL, SAVE_LABEL } from '../../../../constants/labels';
import { FUNC_IS_REQUIRED_TYPE } from '../../../../constants/propTypes';

import './style.css';

export class EditFormControlButtonsPanel extends Component {

  static propTypes = {
    settingsSaveDisabledReasonText: PropTypes.string,
    onSettingsEditCancel: FUNC_IS_REQUIRED_TYPE,
    onSettingsSave: FUNC_IS_REQUIRED_TYPE
  };

  render() {
    const {
      settingsSaveDisabledReasonText,
      onSettingsEditCancel,
      onSettingsSave
    } = this.props;
    return(
      <div className="edit-form-control-buttons-panel">
        {
          <div className="edit-form-control-buttons-panel__submit-disable-reason-text">
            {settingsSaveDisabledReasonText}
          </div>
        }
        <Button onClick={onSettingsEditCancel} variant="contained">
          {CANCEL_LABEL}
        </Button>
        <Button
            onClick={onSettingsSave}
            disabled={!!settingsSaveDisabledReasonText}
            type="submit"
            variant="contained"
            color="primary"
        >
          {SAVE_LABEL}
        </Button>
      </div>
    );
  }
}