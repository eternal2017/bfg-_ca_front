import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReactTable, { ReactTableDefaults } from 'react-table';
import 'react-table/react-table.css';

import {
  ARRAY_OF_ANY_OBJECTS_TYPE,
  NUMBER_OR_STRING_TYPE
} from '../../constants/propTypes';
import {
  APP_WORK_SPACE_CONTENT_TOP_PADDING_IN_PX,
  TASKS_TABLE_CONTAINER_TOP_PADDING_IN_PX
} from '../../constants/styles';

import { NO_TASKS_FOR_CURRENT_PERIOD_MSG } from '../../messages/tasks';

import './style.css';

export class TasksViewScreen extends Component{

  static propTypes = {
    simulationSessionIdTasksFilter: NUMBER_OR_STRING_TYPE,
    departmentIdTasksFilter: NUMBER_OR_STRING_TYPE,
    equipmentClassIdTasksFilter: NUMBER_OR_STRING_TYPE,
    tasksTableColumns: PropTypes.arrayOf(PropTypes.shape({
      Header: PropTypes.string.isRequired,
      accessor: PropTypes.string.isRequired,
      show: PropTypes.bool.isRequired,
      width: PropTypes.number,
      maxWidth: PropTypes.number,
      minWidth: PropTypes.number
    })).isRequired,
    tasksTableData: ARRAY_OF_ANY_OBJECTS_TYPE
  };

  constructor(props){
    super(props);
    this.tableFixedHeight = this._calculateTableHeight();
  }

  /*
  * Для того, чтобы заголовки всегда были на экране, рассчитываем высоту таблицы исходя из клиентского размера экрана,
  * чтобы не появлялся скроллинг браузера, а появлялся скроллинг у таблицы. Вычитаем из высоты экрана размер паддинга
  * рабочей области всего приложения (который учитывает навигационную панель) и размер паддинга у самого компонента
  * таблицы, т.е. получаем размер таблицы от начала её контейнера до конца экрана.
  * Высота определяется только при маунте компонента и не пересчитывается при перерисовках.
  * */
  _calculateTableHeight = () => {
    const clientHeight = document.documentElement.clientHeight;
    return clientHeight -
      APP_WORK_SPACE_CONTENT_TOP_PADDING_IN_PX -
      TASKS_TABLE_CONTAINER_TOP_PADDING_IN_PX;
  };

  _renderTasksTable = () => {
    const {
      tasksTableColumns,
      tasksTableData
    } = this.props;
    return(
      <div className="task-view-screen__table">
        <ReactTable
            column={{
              ...ReactTableDefaults.column,
              headerClassName: 'task-view-screen__table-column-header',
              className: 'task-view-screen__table-column-cell'
            }}
            columns={tasksTableColumns}
            data={tasksTableData}
            pageSize={tasksTableData.length}
            className="-striped"
            sortable={false}
            resizable={false}
            showPagination={false}
            style={{
              height: this.tableFixedHeight
            }}
            getTheadProps={this._getTheadPropsCb}
            getTbodyProps={this._getTbodyPropsCb}
            getTrGroupProps={this._getTrGroupPropsCb}
        />
      </div>
    );
  };

  _getTheadPropsCb = () => ({
    className: 'task-view-screen__table-head'
  });

  _getTbodyPropsCb = () => ({
    className: 'task-view-screen__table-body'
  });

  _getTrGroupPropsCb = () => ({
    className: 'task-view-screen__table-row-group'
  });

  _renderNoTasksInfo = () =>
    <div className="task-view-screen__no-tasks-text">
      {NO_TASKS_FOR_CURRENT_PERIOD_MSG}
    </div>;

  render(){
    return(
      <div className="task-view-screen">
        {
          !!this.props.tasksTableData.length ?
            this._renderTasksTable() :
            this._renderNoTasksInfo()
        }
      </div>
    );
  }
}