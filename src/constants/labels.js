export const CONTACT_SYSTEM_ADMINISTRATOR_LABEL = 'Обратитесь к системному администратору';

export const RELOAD_LABEL = 'Перезагрузить';

export const GO_BACK_LABEL = 'Назад';

export const ENTITY_NAME_LABEL = 'Наименование';

export const ENTITY_IDENTITY_LABEL = 'Клиентский идентификатор';

export const CHOOSE_DEPARTMENT_LABEL = 'Выберите подразделение';

export const CHOOSE_EQUIPMENT_CLASS_LABEL = 'Выберите класс РЦ';

export const TASKS_LABEL = 'Задания';
export const TASKS_FOR_PERIOD_LABEL = 'Задания на';

export const NO_DATA_LABEL = 'Нет данных';

export const WITHOUT_ORDER_LABEL = 'Без заказа';
export const WITHOUT_ORDER_PRIORITY_LABEL = 'Самый низкий';

export const NOT_PLANNED_LABEL = 'Не запланировано';

export const MAIN_LABEL = 'Основной';
export const ALTERNATE_LABEL = 'Альтернативный';

export const DEPARTMENT_LABEL = 'Подразделение';
export const EQUIPMENT_CLASS_LABEL = 'Класс РЦ';

export const CREATE_LABEL = 'Создать';
export const EDIT_LABEL = 'Редактировать';
export const DELETE_LABEL = 'Удалить';

export const SAVE_LABEL = 'Сохранить';
export const CANCEL_LABEL = 'Отменить';

export const SETTINGS_LABEL = 'Настройки';

export const NOT_SELECTED_LABEL = 'Не выбрано';