/*
* Константы доступных action-точек
* */

export const SERVER_ACTION_POINT = {
  LOGIN: 'login',
  IMPORT: 'import',
  SIMULATION_SESSION_FOR_TASKS: 'primarySimulationSession'
};

/*
* Константы доступных команд для action-точек
* */
export const SERVER_ACTION_COMMAND = {
  LOGIN: 'login'
};