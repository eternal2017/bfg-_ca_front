export const SET_APP_STATUS = 'SET_APP_STATUS';
export const SET_APP_AUTH_SESSION_ID = 'SET_APP_AUTH_SESSION_ID';

export const setAppStatus = appStatus => ({
  type: SET_APP_STATUS,
  appStatus
});

export const setAppAuthSessionId = authSessionId => ({
  type: SET_APP_AUTH_SESSION_ID,
  authSessionId
});