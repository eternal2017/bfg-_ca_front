export const RELATED_MODEL_FIELD_DELIMITER = '__';

export const SECS_IN_MINUTE = 60;
export const SECS_IN_HOUR = 3600;