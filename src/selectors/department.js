import { createSelector } from 'reselect';

import {
  entitiesForModelSelectorFactory,
  entitiesListForModelSelectorFactory
} from '../reducers/entities/selectors';

import { DEPARTMENT_MODEL } from '../constants/models';

import { fieldComparatorFactory, stringComparator } from '../utils/arrayComparators';

export const departmentEntitiesSelector = entitiesForModelSelectorFactory(DEPARTMENT_MODEL);

export const departmentsListSelector = entitiesListForModelSelectorFactory(DEPARTMENT_MODEL);

export const departmentsListSortedByIdentitySelector = createSelector(
  departmentEntitiesSelector,
  departmentEntities => Object
    .values(departmentEntities)
    .sort(fieldComparatorFactory(stringComparator, 'identity'))
);