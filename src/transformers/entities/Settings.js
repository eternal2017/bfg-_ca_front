import { Transformer } from '../Transformer';

import { CA_FRONTEND_SETTINGS_GROUP } from '../../constants/settings';

import _mapValues from 'lodash/mapValues';
import _pick from 'lodash/pick';

const TASKS_TABLE_COLUMNS_DATA_KEYS_TO_SAVE = [
  'visible',
  'order'
];

/*
*
* Для сущности модели settings в БД уникальным ключом является комбинация 'группы настройки' (то, к какой области настройка
* относится) и 'имени настройки'.
* Все настройки для приложения ca_frontend будут храниться в служебной группе CA_FRONTEND_SETTINGS_GROUP, чтобы они
* были уникальны, не пересекались с другими веб-приложениями, использующими модель settings. Т.е. признак уникальности
* настроек у ca_frontend, это имя (поле name)
* */
export class Settings extends Transformer{

  static transformToState(item){
    const {
      name: id,
      value: data
    } = item;
    return {
      id,
      data
    }
  };

  static transformToDb(item){
    const {
      id,
      data
    } = item;
    return {
      group: CA_FRONTEND_SETTINGS_GROUP,
      name: id,
      id: [CA_FRONTEND_SETTINGS_GROUP, id].join('/'),
      value: {
        ...data,
        tasksTableColumns: _mapValues(
          data.tasksTableColumns,
          columnData => _pick(columnData, TASKS_TABLE_COLUMNS_DATA_KEYS_TO_SAVE)
        )
      }
    }
  };
}