import moment from 'moment';

export const HTTP_REQUEST_METHOD = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE'
};

export const HTTP_REQUEST_STATUS = {
  SUCCESS: 'SUCCESS',
  UNAUTHORIZED: 'UNAUTHORIZED',
  FAILED: 'FAILED',
  CONNECTION_ERROR: 'CONNECTION_ERRORS'
};

const DEFAULT_HTTP_REQUEST_HEADERS = {
  'X-Time-Zone': moment().format('Z'),
  'Cache-Control': 'no-cache',
  Pragma: 'no-cache'
};

const DEFAULT_HTTP_REQUEST_WITH_JSON_BODY_HEADERS = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

const makeHttpRequest = (url, options = {}) => {
  const httpRequestOptions = {
    credentials: 'include',
    ...options,
    headers: {...DEFAULT_HTTP_REQUEST_HEADERS, ...options.headers }
  };
  return fetch(url, httpRequestOptions)
    .then(response => Promise.all([response.json(), response.status]))
    .then(([jsonResponse, status]) => ({
      status: getRequestStatus(status),
      response: jsonResponse
    }))
    .catch(() => ({ status: HTTP_REQUEST_STATUS.CONNECTION_ERROR }))
};

const getRequestStatus = statusCode => {
  if(statusCode >= 200 && statusCode < 300) return HTTP_REQUEST_STATUS.SUCCESS;
  if(statusCode === 401) return HTTP_REQUEST_STATUS.UNAUTHORIZED;
  return HTTP_REQUEST_STATUS.FAILED;
};

export const makeGETHttpRequest = (url, options = {}) =>
  makeHttpRequest(url, {...options, method: HTTP_REQUEST_METHOD.GET});

const makeHttpRequestWithJsonBody = (url, body, options = {}) =>
  makeHttpRequest(
    url,
    {
      ...options,
      body,
      headers: {
        ...DEFAULT_HTTP_REQUEST_WITH_JSON_BODY_HEADERS,
        ...options.headers
      }
    }
);

export const makePOSTHttpRequest = (url, body, options = {}) =>
  makeHttpRequestWithJsonBody(url, body, {...options, method: HTTP_REQUEST_METHOD.POST});

export const makePUTHttpRequest = (url, body, options = {}) =>
  makeHttpRequestWithJsonBody(url, body, {...options, method: HTTP_REQUEST_METHOD.PUT});

export const makeDELETEHttpRequest = (url, options = {}) =>
  makeHttpRequest(url, {...options, method: HTTP_REQUEST_METHOD.DELETE});