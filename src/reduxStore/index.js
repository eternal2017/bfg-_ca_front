import { compose, createStore, applyMiddleware } from 'redux';

import { enableBatching } from 'redux-batched-actions';

import { createArrayMiddleware } from './middlewares/arrayMiddleware';

import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware, CALL_HISTORY_METHOD } from 'connected-react-router';

import thunkMiddleware from 'redux-thunk';

import rootReducer from '../reducers';

import { PROD } from '../constants/environment';


export const history = createBrowserHistory();

/*
* Расширяем главный редьюсер для работы с экшенами роутера и для обработки массива экшенов, сгенерированного при помощи
* batchActions (см. arrayMiddleware)
* */
const finalReducer = compose(
  enableBatching,
  connectRouter(history)
)(rootReducer);

/*
* Создаем storeEnhancer с redux-dev-tools и нужными мидлварами
* */
const composeEnhancersFunc = (process.env.NODE_ENV !== PROD && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

/*
* Для удобства, первой мидлварой является arrayMiddleWare, для возможности передачи массива любых экшенов в dispatch.
* Параметр callback нужен, чтобы мидлвара правильно отфильтровала экшены, которые можно выполнить через batchDispatch.
* Эшены роутинга должны быть обработаны своей мидлварой, т.к. для роутинга есть особая логика - экшены с определенным типом роутера,
* изменяют стор не непрямую, мидлвара роутера сначала изменяет объект history, а только потом вызывается отдельный служебный экшен
* для обновления store. Поэтому экшены роутера не должны попадать в batchDispatch.
* */
const isPlainActionForAnotherMiddleware = action => action.type === CALL_HISTORY_METHOD;
const arrayMiddleware = createArrayMiddleware(isPlainActionForAnotherMiddleware);

const storeEnhancer = composeEnhancersFunc(
  applyMiddleware(
    arrayMiddleware,
    routerMiddleware(history),
    thunkMiddleware
  ),
);

export const store = createStore(finalReducer, storeEnhancer);

if (process.env.NODE_ENV !== PROD && module.hot) {
  module.hot.accept('../reducers', () => {
    const nextRootReducer = require('../reducers').default;
    store.replaceReducer(nextRootReducer)
  })
}