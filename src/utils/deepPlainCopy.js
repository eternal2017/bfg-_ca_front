import _flowRight from 'lodash/flowRight';

export const deepPlainCopy = _flowRight(
  JSON.parse,
  JSON.stringify
);