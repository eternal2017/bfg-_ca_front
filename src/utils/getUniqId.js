import uuid from 'uuid/v1';

/*
* Иногда для id необходимо вырезать все '-', т.к. при обработке humps camelize в запросах на сервер они могут
* вырезаться - в результате может быть объект вида
* {
*   '123-123': {id: '123123', ...}
* }
* который имеет id не равный внешнему ключу ('123-123' !== '123123')
* */
export const getUniqId = (shouldCutDashes = false) => {
  const id = uuid();
  return shouldCutDashes ? id.replace(/-/g, '') : id;
};
