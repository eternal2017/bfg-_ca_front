export const SAVE_EQUIPMENT_CLASSES_IN_DEPARTMENT = 'SAVE_EQUIPMENT_CLASSES_IN_DEPARTMENT';
export const CLEAR_ALL_EQUIPMENT_CLASSES_IN_DEPARTMENT_DATA = 'CLEAR_ALL_EQUIPMENT_CLASSES_IN_DEPARTMENT_DATA';

export const saveEquipmentClassesInDepartment = (departmentId, equipmentClassEntities) => ({
  type: SAVE_EQUIPMENT_CLASSES_IN_DEPARTMENT,
  departmentId,
  equipmentClassEntities
});

export const clearAllEquipmentClassesInDepartmentData = () => ({
  type: CLEAR_ALL_EQUIPMENT_CLASSES_IN_DEPARTMENT_DATA
});