import { RELATED_MODEL_FIELD_DELIMITER } from '../../constants/magics';

import _pick from 'lodash/pick';
import _flowRight from 'lodash/flowRight';
import _isString from 'lodash/isString';
import _isArray from 'lodash/isArray';
import _get from 'lodash/get';

import humps from 'humps';

export const REST_REQUEST_TYPE = 'rest';
export const REST_COLLECTION_API_REQUEST_TYPE = [REST_REQUEST_TYPE, 'collection'].join('/');
export const FILTER_BLOCK_BEGIN_RESERVED_SYMBOL = '{';
export const FILTER_BLOCK_END_RESERVED_SYMBOL = '}';
export const INCORRECT_FILTER_VALUES_MSG =
  `При фильтрации запрещено указывать служебные символы: "${FILTER_BLOCK_BEGIN_RESERVED_SYMBOL}" и ` +
  `"${FILTER_BLOCK_END_RESERVED_SYMBOL}". Измените значение фильтров`;
const REST_COLLECTION_API_QUERY_KEYS = [
  'orderBy',
  'with',
  'start',
  'stop',
  'filter'
];

export const transformQueryParamsToCollectionApi = queryParams => {
  const transformedQueryParams = _pick(transformQueryParams(queryParams), REST_COLLECTION_API_QUERY_KEYS);
  return humps.decamelizeKeys(transformedQueryParams);
};


const transformSortParams = queryParams => {
  const { sortBy } = queryParams;
  if(!sortBy) return queryParams;
  return {
    ...queryParams,
    orderBy: sortBy
      .map(({column, params}) => ({
        column: humps.decamelize(column),
        params
      }))
  };
};

const transformWithParams = queryParams => {
  if(!queryParams.with) return queryParams;
  return{
    ...queryParams,
    with: queryParams.with
      .map(withParamValue => {
        if(!isQueryParamWithParamsBlock(withParamValue)) return humps.decamelize(withParamValue);
        const { column, params } = withParamValue;
        return{
          column: humps.decamelize(column),
          params: params
            .map(({key, value}) => ({
              key: humps.decamelize(key),
              value: value
            }))
        }
      })
  };
};

const transformLimitParams = queryParams => {
  const { limit, page = 1 } = queryParams;
  if(!limit) return queryParams;
  return{
    ...queryParams,
    start: limit * (page - 1),
    stop: limit * page
  };
};

export const FILTER_TYPES = {
  EQUALS: 'eq',
  CONTAINS: 'ct',
  GREATER_THAN: 'gt',
  LESS_THAN: 'lt',
  GREATER_OR_EQUAL: 'ge',
  LESS_OR_EQUAL: 'le',
  ONE_OF: 'in',
  IS: 'is',
  IS_NOT: 'ns'
};

export const FILTER_GROUP_TYPES = {
  AND: 'AND',
  OR: 'OR'
};

export const WITH_PARAMS = {
  STRICT: 'withStrict'
};

const transformFilterParams = queryParams => {
  const { filter } = queryParams;
  if(!filter) return queryParams;
  return {
    /*
    * В результате преобразований каждая группа в результирующей заключаются внутрь служебных скобок.
    * Для самого верхнего уровня они не нужны (хотя можно было и оставить, но я решил обрезать первый
    * и последний символ)
    * */
    ...queryParams,
    filter: transformFilterGroup(filter)
      .slice(1, -1)
  }
};

const transformFilterGroup = filterGroup => {
  const {
    filterGroupType,
    filters
  } = filterGroup;
  const groupFilterString = filters
    .map((filterGroupElement) =>
      filterGroupElement.column ?
        transformColumnFilter(filterGroupElement) :
        transformFilterGroup(filterGroupElement)
    )
    .join(FILTER_GROUP_TYPES[filterGroupType].toLowerCase());
  return wrapFilterBlock(groupFilterString);
};

const transformColumnFilter = ({column, filterValue, filterType}) => {
  const columnFilterString = [
    /*
    * Приходится декамелайзить здесь, т.к. значения тоже могут содержать camelCase, а их преобразовывать, очевидно,
    * не нужно. Все фильтры здесь преобразуются в одну строку и понять далее где имя колонки, а где значение уже будет,
    * практически, невозможно.
    *
    * */
    humps.decamelize(column),
    transformFilterValue(filterValue)
  ]
    .join(` ${filterType} `);
  return wrapFilterBlock(columnFilterString);
};

const wrapFilterBlock = filterString => [
  FILTER_BLOCK_BEGIN_RESERVED_SYMBOL,
  filterString,
  FILTER_BLOCK_END_RESERVED_SYMBOL
].join('');

const transformFilterValue = (filterValue) => {
  if(filterValue === null || Array.isArray(filterValue)) return JSON.stringify(filterValue);
  return filterValue;
};

const transformQueryParams = _flowRight(
  transformSortParams,
  transformWithParams,
  transformLimitParams,
  transformFilterParams
);

export const transformModelsInQueryParams = (queryParams, modelsRelations) => {
  if(!modelsRelations) return queryParams;
  return {
    ...queryParams,
    ...transformModelsInSortParams(queryParams, modelsRelations),
    ...transformModelsInWithParams(queryParams, modelsRelations),
    ...transformModelsInFilterParams(queryParams, modelsRelations)
  };
};

const transformModelsInSortParams = ({ sortBy }, modelsRelations) => {
  if(!sortBy) return {};
  return {
    sortBy: sortBy
      .map(columnSortData => ({
        ...columnSortData,
        column: transformColumnName(columnSortData.column, modelsRelations)
      }))
  }
};

const transformModelsInWithParams = (queryParams, modelsRelations) => {
  if(!queryParams.with) return {};
  /*
  * Модели with могут быть заданы как строкой так и блочным параметром в виде объекта, обрабатываем оба возможных случая
  * */
  return {
    with: queryParams.with
      .map(relatedModelData => _isString(relatedModelData) ?
        generateApiModelWithRelations(relatedModelData, modelsRelations) :
        ({
          ...relatedModelData,
          column: generateApiModelWithRelations(relatedModelData.column, modelsRelations)
        })
      )
  }
};

const transformModelsInFilterParams = ({filter}, modelsRelations) => {
  if(!filter) return {};
  return{
    filter: transformModelsInFilterGroup(filter, modelsRelations)
  };
};

const transformModelsInFilterGroup = (filterGroup, modelsRelations) => {
  const {
    filterGroupType,
    filters
  } = filterGroup;
  return {
    filterGroupType,
    filters: filters
      .map((filterGroupElement) =>
        filterGroupElement.column ?
          ({
            ...filterGroupElement,
            column: transformColumnName(filterGroupElement.column, modelsRelations)
          }) :
          transformModelsInFilterGroup(filterGroupElement, modelsRelations))
  }
};

export const transformColumnName = (columnName, modelsRelations) => {
  const isRelatedModelColumn = columnName.includes(RELATED_MODEL_FIELD_DELIMITER);
  if(!isRelatedModelColumn) return columnName;

  const [relatedModel, field] = columnName.split(RELATED_MODEL_FIELD_DELIMITER);
  return [generateApiModelWithRelations(relatedModel, modelsRelations), field].join('.');
};

const generateApiModelWithRelations = (model, modelRelations) => {
  const relations = collectRelations(model, modelRelations);
  /*
  * В методе collectRelations, все отношения модели любого уровня вложенности собираются "с конца" до модели первого
  * уровня, по которой осуществляется запрос (уровни описываются в modelRelations). Например,
  * модель entityRoute (4 уровень вложенности), у неё есть с связь с operation (3 уровень вложенности), у operation есть
  * связь с task (2 уровень вложенности), у таска есть связь с equipment (1 уровень вложенности). CollectRelations
  * пробежавшись по modelRelations, соберет массив [operation', 'task', 'equipment'].
  * Чтобы корректно сформировать модель такой связанной модели, нужно развернуть массив, добавить саму модель и соединить
  * все имена модели через точку, т.е. чтобы получилось equipment.task.operation.entityRoute
  * */
  return relations.reverse().concat(model).join('.')
};

const collectRelations = (model, modelRelations, relations = []) => {
  const relatedModel = modelRelations[model].relates;
  if(!relatedModel) return relations;
  return collectRelations(relatedModel, modelRelations, relations.concat(relatedModel));
};


/*
* значения фильтров не должны содержать служебных символов, иначе будут ошибка при отправке запроса, детектируем
* эти фильтры здесь
* */
const RESERVED_FILTER_SYMBOLS_REG_EXP =
  new RegExp(`^[^${FILTER_BLOCK_BEGIN_RESERVED_SYMBOL}${FILTER_BLOCK_END_RESERVED_SYMBOL}]+$`);

export const areFiltersValuesValid = ({filter}) => {
  if(!filter) return true;
  return areFilterGroupValuesValid(filter, RESERVED_FILTER_SYMBOLS_REG_EXP)
};

const areFilterGroupValuesValid = (filterGroup, reservedSymbolsRegExp) => {
  const { filters } = filterGroup;
  return filters
    .every(filterGroupElement => {
      const { column, filterValue } = filterGroupElement;
      if(!column) return areFilterGroupValuesValid(filterGroupElement, reservedSymbolsRegExp);

      const reservedSymbolTestCb = value => reservedSymbolsRegExp.test(value);
      return _isArray(filterValue) ?
        filterValue.every(reservedSymbolTestCb) :
        reservedSymbolTestCb(filterGroupElement.filterValue);
    })
};

export const isQueryParamWithParamsBlock = queryParamValue => !!_get(queryParamValue, 'params');