import { fetchEntities } from '../reducers/entities/actions';
import { saveEquipmentClassesInDepartment } from '../reducers/equipmentClassesInDepartment/actions';
import { setTasksEquipmentClassFilter } from '../reducers/tasksMainFilters/actions';

import { equipmentClassesInDepartmentEntitiesSelector } from '../reducers/equipmentClassesInDepartment/selectors';
import { simulationSessionTasksFilterSelector } from '../reducers/tasksMainFilters/selectors';

import { TASKS_REQUEST_PERIOD_FILTER } from '../reducers/tasks/actions';
import { FILTER_GROUP_TYPES, FILTER_TYPES } from '../api/restCollectionApi';
import {
  EQUIPMENT_CLASS_MODEL,
  EQUIPMENT_MODEL,
  SIMULATION_EQUIPMENT_MODEL
} from '../constants/models';
import { DEPARTMENTS_WITH_TASKS_REQUEST_MODEL_RELATIONS } from './departments';


export const fetchEquipmentClassesInDepartment = (
  departmentId,
  queryParams = {},
  requestOptions = {}
) =>
  (dispatch, getState) => {

    const areEquipmentClassesEntitiesAlreadyInStore =
      !!equipmentClassesInDepartmentEntitiesSelector(getState(), { departmentId });

    if(areEquipmentClassesEntitiesAlreadyInStore) return Promise.resolve();

    const departmentFilter = {
      column: [EQUIPMENT_MODEL, 'departmentId'].join('__'),
      filterType: FILTER_TYPES.EQUALS,
      filterValue: departmentId
    };

    const finalFilters = queryParams.filter ?
    [
      {...queryParams.filter},
      departmentFilter
    ] :
    [departmentFilter];

    const finalQueryParams = {
      ...queryParams,
      filter: {
        filterGroupType: FILTER_GROUP_TYPES.AND,
        filters: finalFilters
      }
    };

    const finalRequestOptions = {
      ...requestOptions,
      modelRelations: {
        ...requestOptions.modelRelations,
        [EQUIPMENT_MODEL]: {
          level: 1
        }
      }
    };

    return dispatch(fetchEntities(
      EQUIPMENT_CLASS_MODEL,
      finalQueryParams,
      finalRequestOptions
    ))
      .then(({ entities }) =>
        dispatch(saveEquipmentClassesInDepartment(departmentId, entities[EQUIPMENT_CLASS_MODEL])));
  };


/*
* Запрос только тех классов РЦ в подразделении, для которых есть задания на отображаемый период
* */
export const fetchEquipmentClassesInDepartmentWithTasks = departmentId =>
  (dispatch, getState) => {

    const { id: simulationSessionIdTasksFilter } = simulationSessionTasksFilterSelector(getState());

    if(!simulationSessionIdTasksFilter) return Promise.resolve();

    const queryParams = {
      filter: {
        filterGroupType: FILTER_GROUP_TYPES.AND,
        filters: [
          {
            column: [SIMULATION_EQUIPMENT_MODEL, 'simulationSessionId'].join('__'),
            filterType: FILTER_TYPES.EQUALS,
            filterValue: simulationSessionIdTasksFilter
          },
          TASKS_REQUEST_PERIOD_FILTER
        ]
      }
    };

    //Отношения связанных моделей такие же, как и для запроса подразделений, в которых есть задания
    const requestOptions = {
      modelRelations: {...DEPARTMENTS_WITH_TASKS_REQUEST_MODEL_RELATIONS}
    };

    return dispatch(fetchEquipmentClassesInDepartment(departmentId, queryParams, requestOptions));
  };

/*
* В роутинге устанавливаются фильтр заданий по классу РЦ в подразделении в виде его клиентского идентификатора. В url,
* теоретически, может быть написано всё что угодно + данные для которые устанавливался ранее фильтр могут быть изменены.
* Поэтому в определенных случаях необходимо выполнить проверку, есть ли класс РЦ с таким клиентским идентификатором
* в БД, чтобы корректно продолжить работу.
*
* Если такой класс РЦ существует, то устанавливаем фильтр в state. Если нет, то реджектим
* */
export const initTasksEquipmentClassFilterFromRoute = equipmentClassIdentityFromRoute =>
  dispatch => {

    const equipmentClassCheckQueryParams = {
      filter: {
        filterGroupType: FILTER_GROUP_TYPES.AND,
        filters: [{
          column: 'identity',
          filterType: FILTER_TYPES.EQUALS,
          filterValue: equipmentClassIdentityFromRoute
        }]
      }
    };

    return dispatch(fetchEntities(EQUIPMENT_CLASS_MODEL, equipmentClassCheckQueryParams, {isBlockingRequest: false}))
      .then(({entities, responseEntitiesIds, responseMeta }) => {

        if (responseMeta.count === 0)
          return Promise.reject(equipmentClassIdentityFromRoute);

        const equipmentClassId = responseEntitiesIds[EQUIPMENT_CLASS_MODEL][0];
        /*
        * Класс РЦ существует в БД, устанавливаем фильтр в state
        * */
        dispatch(setTasksEquipmentClassFilter(entities[EQUIPMENT_CLASS_MODEL][equipmentClassId]));
        return equipmentClassId
      })
  };