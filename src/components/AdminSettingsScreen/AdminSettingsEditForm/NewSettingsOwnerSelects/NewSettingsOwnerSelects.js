import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Select } from '../../../common/Select/Select';

import {
  CHOOSE_DEPARTMENT_FOR_SETTINGS,
  CHOOSE_EQUIPMENT_CLASS_FOR_SETTINGS,
  ADMIN_SETTINGS_REACT_SELECTS_OVERRIDE_STYLES
} from '../../../../constants/settings';
import { SELECT_OPTIONS_TYPE, FUNC_IS_REQUIRED_TYPE } from '../../../../constants/propTypes';

import './style.css';

export class NewSettingsOwnerSelects extends Component {

  static propTypes = {
    selectedDepartment: PropTypes.string,
    departmentSelectOptions: SELECT_OPTIONS_TYPE.isRequired,
    selectDepartment: FUNC_IS_REQUIRED_TYPE,
    selectedEquipmentClass: PropTypes.string,
    equipmentClassSelectOptions: SELECT_OPTIONS_TYPE.isRequired,
    selectEquipmentClass: FUNC_IS_REQUIRED_TYPE,
  };

  _renderDepartmentSelect = () => {
    const {
      selectedDepartment,
      departmentSelectOptions,
      selectDepartment
    } = this.props;
    return(
      <Select
          className="new-settings-owner-selects__department-select"
          selected={selectedDepartment}
          onChange={selectDepartment}
          options={departmentSelectOptions}
          title={CHOOSE_DEPARTMENT_FOR_SETTINGS}
          reactSelectStyles={ADMIN_SETTINGS_REACT_SELECTS_OVERRIDE_STYLES}
          isClearable={false}
      />
    );
  };

  _renderEquipmentClassSelect = () => {
    const {
      selectedDepartment,
      selectedEquipmentClass,
      equipmentClassSelectOptions,
      selectEquipmentClass
    } = this.props;
    return(
      <Select
          className="new-settings-owner-selects__equipment-class-select"
          selected={selectedEquipmentClass}
          onChange={selectEquipmentClass}
          options={equipmentClassSelectOptions}
          title={CHOOSE_EQUIPMENT_CLASS_FOR_SETTINGS}
          reactSelectStyles={ADMIN_SETTINGS_REACT_SELECTS_OVERRIDE_STYLES}
          isClearable
          isDisabled={!selectedDepartment}
      />
    );
  };

  render() {
    return (
      <div className="new-settings-owner-selects">
        {this._renderDepartmentSelect()}
        {this._renderEquipmentClassSelect()}
      </div>
    );
  }
}