export const SIMULATION_SESSION_MODEL = 'simulationSession';

export const DEPARTMENT_MODEL = 'department';
export const EQUIPMENT_CLASS_MODEL = 'equipmentClass';
export const EQUIPMENT_MODEL = 'equipment';
export const OPERATION_MODEL = 'operation';
export const ENTITY_ROUTE_MODEL = 'entityRoute';
export const ENTITY_MODEL = 'entity';
export const ORDER_MODEL = 'order';

export const SIMULATION_EQUIPMENT_MODEL = 'simulationEquipment';
export const SIMULATION_OPERATION_TASK_EQUIPMENT_MODEL = 'simulationOperationTaskEquipment';
export const SIMULATION_OPERATION_TASK_MODEL = 'simulationOperationTask';
export const SIMULATION_ENTITY_BATCH_MODEL = 'simulationEntityBatch';
export const SIMULATION_ORDER_ENTITY_BATCH_MODEL = 'simulationOrderEntityBatch';


export const SETTINGS_MODEL = 'settings';