import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { AdminSettingsControlPanel } from './AdminSettingsControlPanel/AdminSettingsControlPanel';
import { AdminSettingsEditForm } from './AdminSettingsEditForm/AdminSettingsEditForm';

import { APP_STATUS } from '../../reducers/appState/reducer';

import { TASKS_TABLE_COLUMNS } from '../../tableColumns/tasksTableColumns';

import {
  FUNC_IS_REQUIRED_TYPE,
  SELECTED_ADMIN_SETTINGS_DATA_TYPE,
  OBJECT_OF_ANY_TYPE, SELECT_OPTIONS_TYPE
} from '../../constants/propTypes';
import {
  ADMIN_SETTINGS_VIEW_MODE,
  DEPARTMENT_SHOULD_BE_CHOOSEN_FOR_SETTINGS,
  SETTINGS_FOR_DEPARTMENT_ALREADY_EXISTS,
  SETTINGS_FOR_EQUIPMENT_CLASS_IN_DEPARTMENT_ALREADY_EXISTS
} from '../../constants/settings';
import { DEFAULT_TASKS_TYPES_SETTINGS } from '../../constants/tasks';

import { ERROR_BY_APP_INITIALIZATION_FOR_ADMIN_MSG } from '../../messages/appState';
import { CONFIRM_ON_LEAVE_ADMIN_SETTINGS_SCREEN_MSG } from '../../messages/adminSettings';

import { deepPlainCopy } from '../../utils/deepPlainCopy';

import './style.css';

export class AdminSettingsScreen extends Component{

  static propTypes = {
    appStatus: PropTypes.oneOf(Object.values(APP_STATUS)).isRequired,
    isMainSettingsSelected: PropTypes.bool.isRequired,
    selectedSettingsId: PropTypes.string.isRequired,
    selectedSettingsData: SELECTED_ADMIN_SETTINGS_DATA_TYPE,
    settingsSelectOptions: SELECT_OPTIONS_TYPE.isRequired,
    selectSettings: FUNC_IS_REQUIRED_TYPE,
    deleteSelectedSettings: FUNC_IS_REQUIRED_TYPE,
    setDepartmentForEquipmentClassSelectOptions: FUNC_IS_REQUIRED_TYPE,
    departmentOptions: SELECT_OPTIONS_TYPE.isRequired,
    fetchEquipmentClassesInDepartment: FUNC_IS_REQUIRED_TYPE,
    equipmentClassOptions: SELECT_OPTIONS_TYPE.isRequired,
    saveSettings: FUNC_IS_REQUIRED_TYPE,
    settingsEntities: PropTypes.objectOf(OBJECT_OF_ANY_TYPE).isRequired
  };

  /*
  * 1. eslint-plugin-react пока не воспринимает статический метод жизненного цикла, поэтому хочет, чтобы он был
  * до конструктора, но это не логично,т.к. в конструкторе инициализируется state, использующийся в
  * getDerivedStateFromProps. Это, скорее всего, будет исправлено (https://github.com/yannickcr/eslint-plugin-react/issues/1839),
  * поэтому пока игнорируем строку
  * 2. По идее, getDerivedStateFromProps вызывается и при initial рендере и возвращает сам state, т.е. конструктор,
  * теоретически, можно было и не определять. С другой стороны, в этом случае в getDerivedStateFromProps второй аргумент
  * равен null и нужно делать дополнительные проверки. Кроме того, в общем случае, если не определить state в
  * конструкторе, то обязательно нужно предусмотреть, чтобы getDerivedStateFromProps вернул не null при initial рендере,
  * т.к. иначе state не будет инициализирован. Реокмендованный способ (в том числе для читаемости) - в конструкторе
  * инициализировать state "пустыми данными", а в getDerivedStateFormProps заполнять их
  * */
  //eslint-disable-next-line
  constructor(props){
    super(props);
    this.state = {
      selectedSettingsId: null,
      viewMode: ADMIN_SETTINGS_VIEW_MODE.READ_ONLY,
      selectedDepartmentIdentity: null,
      selectedDepartmentName: null,
      selectedEquipmentClassIdentity: null,
      selectedEquipmentClassName: null,
      tasksTypes: {},
      tasksTableColumns: {}
    };
  }

  static getDerivedStateFromProps = (props, state) => {
    if (props.selectedSettingsId === state.selectedSettingsId) return null;
    return AdminSettingsScreen._getInitialStateForSelectedSettingsFromProps(props);
  };

  static _getInitialStateForSelectedSettingsFromProps = props => {
    const {
      selectedSettingsId,
      selectedSettingsData: {
        tasksTypes,
        tasksTableColumns
      }
    } = props;
    return {
      /*
      * храним в локальном state дублирующую информацию об идентификаторе заселекченных настроек, т.к. это необходимо
      * для сравнения в getDerivedStateFromProps (Это рекомендуемый способ из доков реакта, т.к. в  getDerivedStateFromProps
      * осознанно нет доступа к prevProps и не будет)
      */
      selectedSettingsId,
      viewMode: ADMIN_SETTINGS_VIEW_MODE.READ_ONLY,
      selectedDepartmentIdentity: null,
      selectedDepartmentName: null,
      selectedEquipmentClassIdentity: null,
      selectedEquipmentClassName: null,
      tasksTypes: {...tasksTypes},
      tasksTableColumns: deepPlainCopy(tasksTableColumns)
    }
  };

  componentDidMount() {
    window.addEventListener('beforeunload', this._confirmOnLeaveAdminSettingsScreen);
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this._confirmOnLeaveAdminSettingsScreen);
  }

  _confirmOnLeaveAdminSettingsScreen = e => {
    if(this._isReadOnlyViewMode()) return;
    e.returnValue = CONFIRM_ON_LEAVE_ADMIN_SETTINGS_SCREEN_MSG;
  };

  _isViewModeEqualsToCbFactory = viewMode => () => this.state.viewMode === viewMode;
  _isReadOnlyViewMode = this._isViewModeEqualsToCbFactory(ADMIN_SETTINGS_VIEW_MODE.READ_ONLY);
  _isCreatingViewMode = this._isViewModeEqualsToCbFactory(ADMIN_SETTINGS_VIEW_MODE.CREATE);

  _renderAppIsNotInitializedInfo = () =>
    <div className="admin-settings-screen__app-is-not-initialized-info">
      {ERROR_BY_APP_INITIALIZATION_FOR_ADMIN_MSG}
    </div>;

  _renderAppSettingsScreenContent = () =>
    <React.Fragment>
      {this._renderSettingsControlPanel()}
      {this._renderSelectedSettingsForm()}
    </React.Fragment>;

  _renderSettingsControlPanel = () => {
    const {
      selectedSettingsId,
      settingsSelectOptions,
      selectSettings,
      deleteSelectedSettings,
      isMainSettingsSelected
    } = this.props;
    return(
      <AdminSettingsControlPanel
          isDisabled={!this._isReadOnlyViewMode()}
          startNewSettingsCreating={this._handleStartNewSettingsCreating}
          startNewSettingsCreatingUsingSelectedAsTemplate={this._handleCreateNewSettingsUsingSelectedAsTemplate}
          selectedSettingsId={selectedSettingsId}
          settingsSelectOptions={settingsSelectOptions}
          selectSettings={selectSettings}
          startSettingsEditing={this._handleStartSettingsEditing}
          deleteSelectedSettings={deleteSelectedSettings}
          isDeleteSettingsButtonVisible={!isMainSettingsSelected}
      />
    );
  };

  _handleStartNewSettingsCreating = () =>
    this.setState({
      viewMode: ADMIN_SETTINGS_VIEW_MODE.CREATE,
      tasksTypes: {...DEFAULT_TASKS_TYPES_SETTINGS},
      tasksTableColumns: deepPlainCopy(TASKS_TABLE_COLUMNS)
    });

  _handleCreateNewSettingsUsingSelectedAsTemplate = () =>
    this.setState({
      viewMode: ADMIN_SETTINGS_VIEW_MODE.CREATE
    });

  _handleStartSettingsEditing = () =>
    this.setState({
      viewMode: ADMIN_SETTINGS_VIEW_MODE.EDIT
    });

  _renderSelectedSettingsForm = () => {
    const {
      departmentOptions,
      equipmentClassOptions,
    } = this.props;
    const {
      selectedDepartmentIdentity,
      selectedEquipmentClassIdentity,
      tasksTypes,
      tasksTableColumns
    } = this.state;
    return(
      <AdminSettingsEditForm
          isReadOnly={this._isReadOnlyViewMode()}
          isCreating={this._isCreatingViewMode()}
          selectedDepartment={selectedDepartmentIdentity}
          departmentSelectOptions={departmentOptions}
          selectDepartment={this._handleDepartmentSelect}
          selectedEquipmentClass={selectedEquipmentClassIdentity}
          equipmentClassSelectOptions={equipmentClassOptions}
          selectEquipmentClass={this._handleEquipmentClassSelect}
          tasksTypesSettings={tasksTypes}
          editTasksTypesSettings={this._handleTasksTypesSettingsEdit}
          tasksTableColumnsSettings={tasksTableColumns}
          editTasksTableColumnsSettings={this._handleTasksTableColumnsSettingsEdit}
          settingsSaveDisabledReasonText={this._getNewSettingsSaveDisabledReasonText()}
          onSettingsEditCancel={this._handleEditingCancel}
          onSettingsSave={this._handleSaveSettings}
      />
    );
  };

  _handleDepartmentSelect = (_, { departmentId, departmentIdentity, departmentName }) => {
    const {
      fetchEquipmentClassesInDepartment,
      setDepartmentForEquipmentClassSelectOptions,
    } = this.props;

    fetchEquipmentClassesInDepartment(departmentId)
      .then(() => {
        /*
        * подразделение в redux store нужно, чтобы правильно получить опции для классов РЦ в подразделении
        * из контейнера.
        * */
        setDepartmentForEquipmentClassSelectOptions(departmentId);

        this.setState({
          selectedDepartmentIdentity: departmentIdentity,
          selectedDepartmentName: departmentName,
          selectedEquipmentClassIdentity: null,
          selectedEquipmentClassName: null
        });
      });
  };

  _handleEquipmentClassSelect = (_, { equipmentClassIdentity = null, equipmentClassName = null }) =>
    this.setState({
      selectedEquipmentClassIdentity: equipmentClassIdentity,
      selectedEquipmentClassName: equipmentClassName
    });

  _handleTasksTypesSettingsEdit = newTasksTypesSettings =>
    this.setState({
      tasksTypes: newTasksTypesSettings
    });

  _handleTasksTableColumnsSettingsEdit = newTasksTableColumnsSettings =>
    this.setState({
      tasksTableColumns: newTasksTableColumnsSettings
    });

  _getNewSettingsSaveDisabledReasonText = () => {
    const {
      selectedDepartmentIdentity,
      selectedEquipmentClassIdentity
    } = this.state;
    const { settingsEntities } = this.props;

    const newSettingsIdToSave = [selectedDepartmentIdentity, selectedEquipmentClassIdentity]
      .filter(ownerIdentity => ownerIdentity !== null)
      .join('_');

    switch(true){
    case !this._isCreatingViewMode():
      return null;
    case !selectedDepartmentIdentity:
      return DEPARTMENT_SHOULD_BE_CHOOSEN_FOR_SETTINGS;
    case !!settingsEntities[newSettingsIdToSave]:
      return selectedEquipmentClassIdentity === null ?
        SETTINGS_FOR_DEPARTMENT_ALREADY_EXISTS :
        SETTINGS_FOR_EQUIPMENT_CLASS_IN_DEPARTMENT_ALREADY_EXISTS;
    default:
      return null;
    }
  };

  _handleEditingCancel = () =>
    this.setState(AdminSettingsScreen._getInitialStateForSelectedSettingsFromProps(this.props));

  _handleSaveSettings = () =>
    this._isCreatingViewMode() ?
      this._saveNewSetting() :
      this._editSettings();

  _saveNewSetting = () => {
    const {
      saveSettings,
      selectSettings
    } = this.props;
    const {
      selectedDepartmentIdentity,
      selectedDepartmentName,
      selectedEquipmentClassIdentity,
      selectedEquipmentClassName,
      tasksTypes,
      tasksTableColumns
    } = this.state;
    const newSettingsId = selectedEquipmentClassIdentity ?
      [selectedDepartmentIdentity, selectedEquipmentClassIdentity].join('_') :
      selectedDepartmentIdentity;
    const newSettingsData = {
      departmentIdentity: selectedDepartmentIdentity,
      departmentName: selectedDepartmentName,
      equipmentClassIdentity: selectedEquipmentClassIdentity,
      equipmentClassName: selectedEquipmentClassName,
      tasksTypes,
      tasksTableColumns
    };
    return saveSettings(
      newSettingsId,
      newSettingsData
    )
      .then(({id: settingsId}) => selectSettings(settingsId));
  };

  _editSettings = () => {
    const {
      selectedSettingsId,
      selectedSettingsData,
      saveSettings
    } = this.props;
    const {
      tasksTypes,
      tasksTableColumns
    } = this.state;
    return saveSettings(
      selectedSettingsId,
      {
        ...selectedSettingsData,
        tasksTypes,
        tasksTableColumns
      }
    )
      .then(() => this.setState({
        viewMode: ADMIN_SETTINGS_VIEW_MODE.READ_ONLY
      }));
  };

  render(){
    const { appStatus } = this.props;
    return(
      <div className="admin-settings-screen">
        {
          appStatus === APP_STATUS.NOT_INITIALIZED ?
            this._renderAppIsNotInitializedInfo() :
            this._renderAppSettingsScreenContent()
        }
      </div>
    );
  };
}