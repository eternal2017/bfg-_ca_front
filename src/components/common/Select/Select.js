import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReactSelect from 'react-select';

import {
  NUMBER_OR_STRING_TYPE,
  FUNC_IS_REQUIRED_TYPE,
  SELECT_OPTIONS_TYPE,
  OBJECT_OF_ANY_TYPE
} from '../../../constants/propTypes';
import { NO_DATA_LABEL, NOT_SELECTED_LABEL } from '../../../constants/labels';

import _get from 'lodash/get';

import cn from 'classnames';

export class Select extends Component {

  static propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    selected: NUMBER_OR_STRING_TYPE,
    onChange: FUNC_IS_REQUIRED_TYPE,
    options: SELECT_OPTIONS_TYPE,
    isClearable: PropTypes.bool,
    isDisabled: PropTypes.bool,
    placeHolderText: PropTypes.string,
    reactSelectStyles: OBJECT_OF_ANY_TYPE,
    backspaceRemovesValue: PropTypes.bool
  };

  static defaultProps = {
    isClearable: true,
    isDisabled: false,
    placeHolderText: NOT_SELECTED_LABEL,
    backspaceRemovesValue: false
  };

  _getSelectValue = () => {
    const {
      options,
      selected
    } = this.props;
    /*
    * у react-select, в качестве пропа value, должен быть сам объект опции, находим его в массиве опций, если не находим,
    * то устанавливаем в null. Важно, чтобы не было undefined, чтобы селект всегда был controlled
    * */
    const selectedOption = options.find(({optionValue}) => optionValue === selected);
    return selectedOption || null;
  };

  _selectChangeCbFactory = (selected, onChangeCb) =>
    newSelectedOption => {
      const newSelectedValue = _get(newSelectedOption, 'optionValue', null);
      const newSelectedOptionPayload = _get(newSelectedOption, 'optionPayload', {});
      if(selected === newSelectedValue) return;
      onChangeCb(newSelectedValue, newSelectedOptionPayload);
    };

  _getSelectOptionValue = ({optionValue}) => optionValue;

  _getSelectOptionLabel = ({optionLabel}) => optionLabel;

  _noOptionsMessageCb = () => NO_DATA_LABEL;

  render() {
    const {
      className,
      title,
      selected,
      onChange,
      options,
      isClearable,
      isDisabled,
      placeHolderText,
      reactSelectStyles,
      backspaceRemovesValue
    } = this.props;
    return (
      <div className={cn(className)}>
        {title}
        <ReactSelect
            value={this._getSelectValue()}
            onChange={this._selectChangeCbFactory(selected, onChange)}
            options={options}
            isClearable={isClearable}
            isDisabled={isDisabled}
            getOptionValue={this._getSelectOptionValue}
            getOptionLabel={this._getSelectOptionLabel}
            noOptionsMessage={this._noOptionsMessageCb}
            placeholder={placeHolderText}
            styles={reactSelectStyles}
            backspaceRemovesValue={backspaceRemovesValue}
        />
      </div>
    );
  }
}