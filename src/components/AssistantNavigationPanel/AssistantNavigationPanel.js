import React, { Component } from 'react';

import PropTypes from 'prop-types';

import Refresh from '@material-ui/icons/Refresh';
import ArrowBack from '@material-ui/icons/ArrowBack';

import { TASKS_REQUEST_PERIOD_IN_HOURS } from '../../reducers/tasks/actions';
import {
  CHOOSE_DEPARTMENT_LABEL,
  CHOOSE_EQUIPMENT_CLASS_LABEL,
  GO_BACK_LABEL,
  RELOAD_LABEL,
  TASKS_FOR_PERIOD_LABEL,
  TASKS_LABEL
} from '../../constants/labels';
import { FORMAT_FULL_DATE } from '../../constants/dateFormats';

import _isFunction from 'lodash/isFunction';

import cn from 'classnames';
import moment from 'moment';

import './style.css';


export class AssistantNavigationPanel extends Component{

  static propTypes = {
    appErrorTitle: PropTypes.string,
    tasksPeriodStartDate: PropTypes.string,
    departmentTitle: PropTypes.string,
    equipmentClassTitle: PropTypes.string,
    onGoBackButtonClick: PropTypes.func,
    choosenDepartmentFullName: PropTypes.string,
    choosenEquipmentClassName: PropTypes.string
  };

  _renderGoBackButton = () => {
    const { onGoBackButtonClick } = this.props;
    const isGoBackButtonInvisible = !_isFunction(onGoBackButtonClick);
    return this._renderControlButton({
      className: cn(
        'assistant-navigation-panel__go-back-button',
        {'assistant-navigation-panel__go-back-button--invisible': isGoBackButtonInvisible}
      ),
      title: GO_BACK_LABEL,
      IconComponent: ArrowBack,
      onClick: onGoBackButtonClick
    })
  };

  _renderControlButton = ({
    className,
    title,
    IconComponent,
    onClick
  }) =>
    <div
        className={cn(
          'assistant-navigation-panel__control-button',
          className
        )}
        onClick={onClick}
    >
      <IconComponent className="assistant-navigation-panel__control-button-icon"/>
      <div className="assistant-navigation-panel__control-button-text">
        {title}
      </div>
    </div>;

  _renderTitles = () => {
    return(
      <div className="assistant-navigation-panel__titles">
        {this._renderTasksTitle()}
        {this._renderEquipmentClassTitle()}
        {this._renderDepartmentTitle()}
        {this._renderAppErrorTitle()}
      </div>
    );
  };

  _renderTasksTitle = () => {
    const { equipmentClassTitle, appErrorTitle } = this.props;
    return this._renderTitle({
      title: this._getTasksTitleText(),
      isActive: true,
      isInvisible: !equipmentClassTitle || !!appErrorTitle
    });
  };

  _getTasksTitleText = () => {
    const { tasksPeriodStartDate } = this.props;
    if(!tasksPeriodStartDate) return TASKS_LABEL;

    const tasksPeriodStartDateMomentObj = moment(tasksPeriodStartDate);
    const tasksPeriodStartDateFormattedString = tasksPeriodStartDateMomentObj.format(FORMAT_FULL_DATE);

    const tasksPeriodStopDateFormattedString = tasksPeriodStartDateMomentObj
      .add(TASKS_REQUEST_PERIOD_IN_HOURS, 'hours')
      .format(FORMAT_FULL_DATE);

    const tasksPeriodInfo = [
      tasksPeriodStartDateFormattedString,
      tasksPeriodStopDateFormattedString
    ].join(' - ');

    return [
      TASKS_FOR_PERIOD_LABEL,
      tasksPeriodInfo
    ].join(' ')
  };

  _renderEquipmentClassTitle = () => {
    const { departmentTitle, equipmentClassTitle, appErrorTitle } = this.props;
    return this._renderTitle({
      title: equipmentClassTitle || CHOOSE_EQUIPMENT_CLASS_LABEL,
      isActive: !equipmentClassTitle,
      isInvisible: !departmentTitle || !!appErrorTitle
    });
  };

  _renderDepartmentTitle = () => {
    const { departmentTitle, appErrorTitle } = this.props;
    return this._renderTitle({
      title: departmentTitle || CHOOSE_DEPARTMENT_LABEL,
      isActive: !departmentTitle,
      isInvisible: !!appErrorTitle
    });
  };

  _renderAppErrorTitle = () => {
    const { appErrorTitle } = this.props;
    return this._renderTitle({
      title: appErrorTitle,
      isActive: true,
      isInvisible: !appErrorTitle
    });
  };

  _renderTitle = ({title, isActive = false, isInvisible = false}) =>
    <div
        className={cn(
          'assistant-navigation-panel__title',
          {'assistant-navigation-panel__title--active': isActive},
          {'assistant-navigation-panel__title--invisible': isInvisible}
        )}
    >
      {title}
    </div>;

  _renderReloadButton = () =>
    this._renderControlButton({
      className: 'assistant-navigation-panel__reload-button',
      title: RELOAD_LABEL,
      IconComponent: Refresh,
      onClick: () => document.location.reload(true)
    });

  render(){
    return(
      <nav className="assistant-navigation-panel">
        {this._renderGoBackButton()}
        {this._renderTitles()}
        {this._renderReloadButton()}
      </nav>
    );
  }
}