export const TASK_TYPES = {
  OPERATION: 0,
  PREPARING: 1,
  SETUP: 2
};

export const TASK_TYPES_TITLES_MAP = {
  [TASK_TYPES.OPERATION]: 'Операция',
  [TASK_TYPES.PREPARING]: 'П-З работы',
  [TASK_TYPES.SETUP]: 'Наладка'
};

export const TASK_TYPES_DURATION_IN_TECHNOLOGY_FIELD_MAP = {
  [TASK_TYPES.OPERATION]: 'prodTime',
  [TASK_TYPES.PREPARING]: 'prepTime',
  [TASK_TYPES.SETUP]: 'setupTime'
};

export const DEFAULT_TASKS_TYPES_SETTINGS = {
  [TASK_TYPES.OPERATION]: true,
  [TASK_TYPES.PREPARING]: true,
  [TASK_TYPES.SETUP]: true
};