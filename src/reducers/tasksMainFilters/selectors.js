import { createSelector } from 'reselect';

import { DEPARTMENT_LABEL, EQUIPMENT_CLASS_LABEL } from '../../constants/labels';

export const tasksMainFiltersSelector = state => state.tasksMainFilters;

export const simulationSessionTasksFilterSelector = state => tasksMainFiltersSelector(state).simulationSession;

export const departmentTasksFilterSelector = state => tasksMainFiltersSelector(state).department;

export const departmentTasksFilterTitleSelector = createSelector(
  departmentTasksFilterSelector,
  departmentTasksFilter => {
    const { name, identity } = departmentTasksFilter;
    if(name === undefined || identity === undefined) return null;

    return [
      DEPARTMENT_LABEL,
      `${name} (${identity})`
    ].join(': ');
  }
);

export const equipmentClassTasksFilterSelector = state => tasksMainFiltersSelector(state).equipmentClass;

export const equipmentClassTasksFilterTitleSelector = createSelector(
  equipmentClassTasksFilterSelector,
  equipmentClassTasksFilter => {
    const { name, identity } = equipmentClassTasksFilter;
    if(name === undefined || identity === undefined) return null;

    return [
      EQUIPMENT_CLASS_LABEL,
      `${name} (${identity})`
    ].join(': ')
  }
);