export const NO_EQUIPMENT_CLASSES_WITH_TASKS_MSG = 'На текущий момент в системе нет заданий ни для одного из ' +
  'классов РЦ этого подразделения. Выберите другое подразделение';

export const ONLY_EQUIPMENT_CLASSES_WITH_TASKS_DISPLAYED_MSG =
  'В системе не отображаются классы РЦ в подразделении, для которых не были сформированы задания';