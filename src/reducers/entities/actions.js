import { sendNotification } from '../notification/actions';
import { blockingAsyncActionWrap } from '../blockingAsyncAction/actions';
import { deleteData, fetchData, postData, showServerError } from '../../api/httpRequestsApi/actions';

import {
  areFiltersValuesValid,
  INCORRECT_FILTER_VALUES_MSG,
  REST_REQUEST_TYPE,
  REST_COLLECTION_API_REQUEST_TYPE,
  transformModelsInQueryParams,
  transformQueryParamsToCollectionApi
} from '../../api/restCollectionApi';

import { transformEntitiesToState, transformEntitiesToDb } from '../../transformers/entities';

import { NOTIFICATION_LEVEL } from '../../constants/notification';
import { HTTP_REQUEST_METHOD } from '../../api/httpRequestsApi';

import { normalizeData } from '../../utils/normalizeData';

import _flow from 'lodash/flow';
import _omit from 'lodash/omit';
import _isString from 'lodash/isString';
import _mapValues from 'lodash/mapValues';
import _size from 'lodash/size';
import _partition from 'lodash/partition';
import _keyBy from 'lodash/keyBy';

const { BACKEND_SERVER_HOST } = window.config;

export const ADD_ENTITIES_TO_STORE = 'ADD_ENTITIES_TO_STORE';
export const DELETE_ENTITIES_FROM_STORE = 'DELETE_ENTITIES_FROM_STORE';
export const DELETE_ALL_MODEL_ENTITIES_FROM_STORE = 'DELETE_ALL_MODEL_ENTITIES_FROM_STORE';

/**
 * Основной экшн записи сущностей полученных их БД через REST в state.entities
 *
 * @param entitiesData {Object} Нормализованные данные моделей вида:
 * {
 *   model1: {
 *     [id1]: {
 *       id: id1,
 *       ...другие поля сущности
 *     },
 *     [id2]: {
 *       id: id2,
 *       ...другие поля сущности
 *     },
 *     ...другие сущности
 *   },
 *   model2: {
 *     ...аналогично
 *   },
 *   ...другие модели
 *
 * }
 *
 */
export const  addEntitiesToStore = entitiesData => ({
  type: ADD_ENTITIES_TO_STORE,
  entitiesData
});

export const deleteEntitiesFromStore = (model, ids) => ({
  type: DELETE_ENTITIES_FROM_STORE,
  model,
  ids
});

export const deleteAllModelEntitiesFromStore = model => ({
  type: DELETE_ALL_MODEL_ENTITIES_FROM_STORE,
  model
});


/**
 * Получение сущностей определенной модели фронта через REST используя API коллекций.
 * Полученные данные обрабатываются трансформерами (подробное описание в Transformer.js) и нормализуются
 * Если в запросе указываются связанные модели, то все преобразования проводятся также и по ним
 *
 * @param model {String} Модель фронта создаваемой \ редактируемой сущности (не обязательно должна быть равна модели в БД).
 * На основании этой модели, параметра options и queryParams формируется правильный запрос.
 * @param queryParams {Object} GET параметры запроса:
 * {
 *   sortBy: [      множественная сортировка
 *     {
 *        column: {String}  наименование колонки БД основной или связанной модели,
 *        params: [{key: 'asc', value: true|false}] - параметры сортировки, пока только направление сортировки
 *     },
 *     ...{параметры следующих колонок для множественной сортировки, если необходимо}
 *   ],
 *   limit: {number} количество запрашиваемых записей модели, если не задано или null\ 0, то все записи независимо от значения page
 *   page: {number} страница, с которой получать записи. Работает вместе с limit, если limit не falsy.
 *   Например: нужны записи с 10 по 20 - limit = 10, page = 2
 *   filter: {      фильтрация
 *     filterGroupType: FILTER_GROUP_TYPES.AND|OR]    -  указатель группы фильтров (по И \ ИЛИ)
 *     filters: [  - данные фильтров группы, которые будет объединены по И \ ИЛИ согласно типу группы, указанному выше
 *         {
 *           column: {String}, наименование колонки БД или связанной модели
 *           filterType: {String}, тип фильтра, константы FILTER_TYPES
 *           filterValue: {String|Number}, значение фильтра
 *         },
 *         {
 *           ...данные фильтра по другой колонке группы фильтра
 *         },
 *         {
 *           filterGroupType: FILTER_GROUP_TYPES.AND|OR]    - вложенная группа фильтров
 *           filters: [
 *             ... данные фильтров вложенной группы
 *           ]
 *         }
 *         ...
 *       ]
 *   },
 *   with: [   связанные модели. Элемент массива либо строка, либо объект
 *     'superModel' {String},  наименование связанной модели
 *    {
 *       column: {String}, наименование связанной модели
 *       params: [{key: WITH_PARAMS.STRICT, value: false}] - параметры связанной модели. Пока это только возможность
 *       указания строгой и нестрогой стыковки связанной модели. Сейчас, по умолчанию, API стыкует строго, т.е. если
 *       так и надо, то связанную модель можно просто задавать строкой. Если нужна нестрогая стыковка то задавать
 *       связанную модель нужно в описанном блочном виде [{key: WITH_PARAMS.STRICT, value: false}].
 *
 *    },
 *    ...
 *   ]
 *
 *   Пример стыковки:
 *       Исходные данные:
 *       entity_route:[
 *         {id: 1, ...params},
 *         {id: 2, ...params}
 *       ],
 *       operation: {
 *         {id: 3, entity_route_id: 1, ...params},
 *         {id: 3, entity_route_id: 1, ...params},
 *       }
 *
 *       Строгая стыковка:
 *       Запрос модели entity_route с параметром with = ['operation'] или [{column: 'operation', [{key: WITH_PARAMS.STRICT, value: true}]}].
 *       Выборка в ответе:
 *       entity_route:[
 *         {id: 1, ...params},
 *       ],
 *       operation: {
 *         {id: 3, entity_route_id: 1, ...params},
 *         {id: 3, entity_route_id: 1, ...params},
 *       }
 *
 *       Нестрогая стыковка:
 *       Запрос модели entity_route с параметром with = [{column: 'operation', [{key: WITH_PARAMS.STRICT, value: false}]}].
 *       Выборка в ответе:
 *       entity_route:[
 *         {id: 1, ...params},
 *         {id: 2, ...params}
 *       ],
 *       operation: {
 *         {id: 3, entity_route_id: 1, ...params},
 *         {id: 3, entity_route_id: 2, ...params},
 *       }
 *
 * }
 *
 * @param options {Object} Дополнительные опции для формирования запроса.
 *  - options.dbModel {String} модель БД, сущности которой запрашиваются. Если не задано, то, по-умолчанию, модель
 *  БД принимается равной первому параметру model. Т.е. задавать этот параметр следует, только если названия модели
 *  фронта и модели в БД отличаются.
 *  - options.modelRelations {Object} объект, описывающий отношения связанных моделей к основной и структуру вложенности,
 *  (что на что ссылается по уровням вложенности) для конкретного запроса (API коллекций гибкое, поэтому одни и те же данные, теоретически,
 *  можно получить с разных REST-точек виртуозно манипулируя параметрами и стыкуя модели, как душа пожелает, именно
 *  поэтому в этой 'абстракции' отношения задаются под конкретный запрос). На основании этого описания отношений моделей
 *  будут преобразованы названия колонок связанных моделей любого уровня вложенности в GET параметрах (queryParams).
 *  Если никакие GET-параметры не нужны или есть уверенность, что ссылки на колонки в GET-параметрах - это колонки
 *  основной модели или модели первого уровня вложенности или же полные наименования связанных колонок в GET параметрах
 *  сформированы  вручную, то modelRelations задавать незачем.
 *  - Также, при помощи options, если необходимо, можно задать параметры низкоуровневой абстракции выполнения http GET
 *  запроса - fetchData
 *
 * В результате выполнения thunk'а возвращается Promise c объектом ответа на GET запрос вида:
 * {
 *   responseEntitiesIds: массив идентификаторов запрашиваемых сущностей, определяющих порядок полученных сущностей в ответе
 *   entities - трансформированные и нормализованные данные
 *   responseMeta - мета информация по запросу
 * }
 * или объект ошибки, если запрос не удался
 */
const DEFAULT_FETCH_ENTITIES_REQUEST_OPTIONS = {
  isBlockingRequest: true
};
export function fetchEntities(model, queryParams = {}, options = {}) {

  return (dispatch, getState) => {

    const fetchEntitiesRequestOptions = {
      ...DEFAULT_FETCH_ENTITIES_REQUEST_OPTIONS,
      ...options
    };

    const fetchEntitiesAsyncCb = () => {

      if (!areFiltersValuesValid(queryParams)) {
        dispatch(sendNotification(INCORRECT_FILTER_VALUES_MSG, NOTIFICATION_LEVEL.ERROR));
        return Promise.reject(INCORRECT_FILTER_VALUES_MSG);
      }

      const {
        dbModel,
        modelRelations
      } = fetchEntitiesRequestOptions;

      /*
      * если определено options.dbModel, то модели сущности фронта и ДБ не совпадают, запрос делаем на модель options.dbModel,
      * а записаны данные будут по ключу фронтовой модели model
      */
      const modelForRest = dbModel || model;

      const requestUrl = [
        BACKEND_SERVER_HOST,
        REST_COLLECTION_API_REQUEST_TYPE,
        modelForRest
      ].join('/');


      const transformedQueryParams = _flow(
        transformModelsInQueryParams,
        transformQueryParamsToCollectionApi,
      )(queryParams, modelRelations);

      return dispatch(fetchData(
        requestUrl,
        transformedQueryParams,
        {...fetchEntitiesRequestOptions, isBlockingRequest: false}
      ))
        .then(response => {

          const responseData = {
            ..._omit(response, [modelForRest, 'meta']),
            [model]: response[modelForRest]
          };

          const state = getState();

          const transformedResponseData = _mapValues(
            responseData,
            (modelEntitiesArray, modelIdentity) => transformEntitiesToState(modelEntitiesArray, modelIdentity, state)
          );

          const requestWithParams = queryParams.with || [];

          const responseModels = [
            model,
            ...requestWithParams
              .map(relatedModelData => _isString(relatedModelData) ? relatedModelData : relatedModelData.column)
          ];

          const { result: responseEntitiesIds, entities } = normalizeData(transformedResponseData, responseModels);
          return { responseEntitiesIds, entities, responseMeta: response.meta };
        });
    };

    return fetchEntitiesRequestOptions.isBlockingRequest ?
      dispatch(blockingAsyncActionWrap(fetchEntitiesAsyncCb)) :
      fetchEntitiesAsyncCb();
  }
}

/*
* Зачастую сразу после получения необходимо записать данные в общее хранилище для entities в store,
* поэтому выделено в отдельный экшен
* */
export const fetchEntitiesAndAddToStore = (model, queryParams = {}, options = {}) =>
  dispatch =>
    dispatch(fetchEntities(model, queryParams, options))
      .then(({ entities }) => dispatch(addEntitiesToStore(entities)));



/**
 * Выполняет POST или PUT на REST-точки БД
 * @param model {string} Модель фронта создаваемой \ редактируемой сущности (не обязательно должна быть равна модели в БД).
 * На основании этой модели и параметра options формируется правильный запрос. Данные ответа в нормализованном виде
 * будут записаны в state.entities[model]
 * @param dataArr {Array} Данные которые будут переданы в теле POST/PUT запроса, для удобства всегда оперируем коллекциями
 * @param isCreating {boolean} Флаг создания \ редактирования: true, если данные создаются (POST), false если редактируются (PUT)
 * @param options {Object} Дополнительные опции для формирования запроса.
 *  - options.dbModel - модель БД, сущности которой создаются \ редактируются. Если не задано, то, по-умолчанию, модель
 *  БД принимается равной первому параметру model. Т.е. задавать этот параметр следует, только если названия модели
 *  фронта и модели в БД отличаются.
 *  - Также, при помощи options, если необходимо, можно задать параметры низкоуровневой абстракции postData
 *
 * В результате выполнения thunk'а возвращается Promise c массивом удачно созданных \ отредактированных (трансформированных)
 * объектов модели или массив объектов ошибок в реджекнутом Promise, если при создании \ редактировании каждого из
 * элементов коллекции возникли ошибки
 */
const DEFAULT_SAVE_ENTITIES_REQUEST_OPTIONS = {
  isBlockingRequest: false,
  showServerError: true
};
export const saveEntities = (model, dataArr, isCreating = true, options = {}) =>

  (dispatch, getState) => {

    /*
    * Не отправляем запросы с пустым телом
    * */
    if(!_size(dataArr)) return Promise.resolve({[model]: []});

    const saveEntitiesRequestOptions = {
      ...DEFAULT_SAVE_ENTITIES_REQUEST_OPTIONS,
      ...options
    };

    const saveEntitiesAsyncCb = () => {

      const state = getState();

      const httpMethod = isCreating ? HTTP_REQUEST_METHOD.POST : HTTP_REQUEST_METHOD.PUT;

      const modelForRest = saveEntitiesRequestOptions.dbModel || model;

      const mainRequestUrl = [
        BACKEND_SERVER_HOST,
        REST_REQUEST_TYPE,
        modelForRest
      ].join('/');

      /*
      * Для PUT запросов при редактировании коллекций в конце url указывается служебный символ '_'
      * */
      const requestUrl = isCreating ?
        mainRequestUrl :
        [mainRequestUrl, '_'].join('/');

      return dispatch(postData(
        requestUrl,
        {
          [modelForRest]: transformEntitiesToDb(dataArr, model, state)
        },
        {
          ...saveEntitiesRequestOptions,
          httpMethod,
          isBlockingRequest: false
        }
      ))
        .then(response => {

          /*
          * В случае ошибок при сохранении \ редактировании коллекций, ошибки приходят в теле ответа и содержат
          * поле _errors
          * */
          const [responseItemsWithErrors, responseItemsWithoutErrors] = _partition(
            response[modelForRest],
            responseItem => '_errors' in responseItem
          );

          if(saveEntitiesRequestOptions.showServerError)
            responseItemsWithErrors.forEach(responseItemWithError => dispatch(showServerError(responseItemWithError)));

          /*
          * Если при сохранении \ редактировании всех элементов коллекции возникли ошибки, то реджектим запрос
          * */
          if(!_size(responseItemsWithoutErrors))
            return Promise.reject(responseItemsWithErrors);

          return transformEntitiesToState(responseItemsWithoutErrors, model, state);

        });
    };

    return saveEntitiesRequestOptions.isBlockingRequest ?
      dispatch(blockingAsyncActionWrap(saveEntitiesAsyncCb)) :
      saveEntitiesAsyncCb();
  };

export const saveEntitiesAndAddToStore = (model, dataArr, isCreating = true, options = {}) =>
  dispatch =>
    dispatch(saveEntities(model, dataArr, isCreating, options))
      .then(entitiesArr => {
        const normalizedEntities = _keyBy(entitiesArr, 'id');
        dispatch(addEntitiesToStore({
          [model]: normalizedEntities
        }));
        return normalizedEntities;
      });


export const deleteEntities = (model, ids, options = {}) =>
  dispatch => {
    if(!_size(ids)) return Promise.resolve([]);
    const mainRequestUrl = [
      BACKEND_SERVER_HOST,
      REST_REQUEST_TYPE,
      model
    ].join('/');
    return Promise.all(
      ids.map(id => dispatch(deleteData([mainRequestUrl, id].join('/'), options)))
    );
  };

export const deleteEntitiesAndDeleteFromStore = (model, ids, options = {}) =>
  dispatch => {
    if(!_size(ids)) return Promise.resolve([]);
    return dispatch(deleteEntities(model, ids, options))
      .then(() => dispatch(deleteEntitiesFromStore(model, ids)));
  };