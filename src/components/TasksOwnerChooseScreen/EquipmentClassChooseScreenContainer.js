import { compose } from 'redux';
import { connect } from 'react-redux';

import { push } from 'connected-react-router'

import { asyncComponent } from '../../hoc/asyncComponent/asyncComponent';

import { fetchEntities } from '../../reducers/entities/actions';
import {
  setTasksEquipmentClassFilter,
  resetTasksEquipmentClassFilter
} from '../../reducers/tasksMainFilters/actions';
import { sendNotification, clearNotificationsByScope } from '../../reducers/notification/actions';

import {
  fetchEquipmentClassesInDepartmentWithTasks
} from '../../operations/equipmentClasses';
import { initTasksDepartmentFilterFromRoute } from '../../operations/departments';

import { equipmentClassesInDepartmentListSortedByIdentitySelector } from '../../selectors/equipmentClassesInDepartment';
import { departmentTasksFilterSelector } from '../../reducers/tasksMainFilters/selectors';

import { EQUIPMENT_CLASS_CHOOSE_SCREEN_NOTIFICATION_SCOPE, NOTIFICATION_LEVEL } from '../../constants/notification';
import { ONLY_EQUIPMENT_CLASSES_WITH_TASKS_DISPLAYED_MSG } from '../../messages/equipmentClass';

import { NO_EQUIPMENT_CLASSES_WITH_TASKS_MSG } from '../../messages/equipmentClass';

import { TasksOwnerChooseScreen } from './TasksOwnerChooseScreen';

const mapStateToProps = state => {
  const {
    id: departmentIdTasksFilter,
    identity: departmentIdentityTasksFilter
  } = departmentTasksFilterSelector(state);
  return {
    equipmentClassesList: equipmentClassesInDepartmentListSortedByIdentitySelector(state, { departmentId: departmentIdTasksFilter}),
    departmentIdTasksFilter,
    departmentIdentityTasksFilter
  };
};

const mapDispatchToProps = {
  fetchEntities,
  fetchEquipmentClassesInDepartmentWithTasks,
  initTasksDepartmentFilterFromRoute,
  push,
  setTasksEquipmentClassFilter,
  resetTasksEquipmentClassFilter,
  sendNotification,
  clearNotificationsByScope
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    equipmentClassesList,
    departmentIdTasksFilter,
    departmentIdentityTasksFilter
  } = stateProps;
  const {
    fetchEquipmentClassesInDepartmentWithTasks,
    initTasksDepartmentFilterFromRoute,
    push,
    setTasksEquipmentClassFilter,
    resetTasksEquipmentClassFilter,
    sendNotification,
    clearNotificationsByScope
  } = dispatchProps;
  return {
    tasksOwnersList: equipmentClassesList
      .map(equipmentClassEntity => {
        const {
          id,
          name: ownerMainTitle,
          identity: ownerSecondaryTitle
        } = equipmentClassEntity;
        return {
          id,
          ownerMainTitle,
          ownerSecondaryTitle,
          ownerOriginalData: equipmentClassEntity
        }
      }),
    resetTasksOwnerInStore: resetTasksEquipmentClassFilter,
    onSelectTasksOwner: (_, equipmentClassEntity) => {
      setTasksEquipmentClassFilter(equipmentClassEntity);
      push(`/${departmentIdentityTasksFilter}/${equipmentClassEntity.identity}`);
    },
    emptyOwnerListMessage: NO_EQUIPMENT_CLASSES_WITH_TASKS_MSG,
    showDisplayedTasksOwnersInfoNotification: () =>
      sendNotification(
        ONLY_EQUIPMENT_CLASSES_WITH_TASKS_DISPLAYED_MSG,
        NOTIFICATION_LEVEL.INFO,
        { autoDismiss: 0, scope: EQUIPMENT_CLASS_CHOOSE_SCREEN_NOTIFICATION_SCOPE}
      ),
    clearDisplayedTasksOwnersInfoNotification: () =>
      clearNotificationsByScope(EQUIPMENT_CLASS_CHOOSE_SCREEN_NOTIFICATION_SCOPE),
    fetchRequiredData: () => {

      /*
      * Если фильтр уже установлен в state при маунте компонента, это означает, что в БД такое подразделение точно есть,
      * проверок на это делать не нужно, просто запрашиваем классы РЦ в нём
      * */
      if(!!departmentIdTasksFilter) return fetchEquipmentClassesInDepartmentWithTasks(departmentIdTasksFilter);

      /*
      * Если же фильтр при маунте не установлен в state, то необходимо инициализировать его из параметра урла, а после
      * этого запросить данные по классам РЦ для этого подразделения. Если инициализировать не удалось (такого подразделения
      * нет в БД), то редиректим на экран выбора подразделений
      * */
      const { departmentIdentity: departmentIdentityFromRoute } = ownProps.match.params;

      return initTasksDepartmentFilterFromRoute(departmentIdentityFromRoute)
        .then(
          departmentId => fetchEquipmentClassesInDepartmentWithTasks(departmentId),
          () => push('/')
        );
    }
  };
};

export const EquipmentClassChooseScreenContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  asyncComponent({
    resolve: [
      {
        fn: ({fetchRequiredData}) => fetchRequiredData()
      }
    ]
  })
)(TasksOwnerChooseScreen);

EquipmentClassChooseScreenContainer.displayName = 'EquipmentClassChooseScreenContainer';