import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import {
  FUNC_IS_REQUIRED_TYPE,
  NUMBER_OR_STRING_TYPE,
  OBJECT_OF_ANY_TYPE
} from '../../../constants/propTypes';

import { NO_DATA_LABEL } from '../../../constants/labels';

import cn from 'classnames';

import './style.css';


export class SimpleItemChoosePanel extends Component {

  static propTypes = {
    itemsList: PropTypes.arrayOf(PropTypes.shape({
      itemId: NUMBER_OR_STRING_TYPE.isRequired,
      itemData: OBJECT_OF_ANY_TYPE,
      itemContent: PropTypes.element.isRequired,
      gridItemWrapClassName: PropTypes.string,
      itemClassName: PropTypes.string
    })),
    onSelectItem: FUNC_IS_REQUIRED_TYPE,
    gridProps: PropTypes.shape({
      direction: PropTypes.oneOf(['row', 'row-reverse', 'column', 'column-reverse']),
      justify: PropTypes.oneOf(['flex-start', 'center', 'flex-end', 'space-between', 'space-around']),
      alignItems: PropTypes.oneOf(['flex-start', 'center', 'flex-end', 'stretch', 'baseline'])
    }),
    noItemsText: PropTypes.string
  };

  static defaultProps = {
    itemsList: [],
    gridProps: {
      justify: 'center'
    },
    noItemsText: NO_DATA_LABEL
  };

  _renderItems = () => {
    const {
      gridProps: { justify, direction, alignItems },
      itemsList
    } = this.props;
    return(
      <Grid container direction={direction} justify={justify} alignItems={alignItems}>
        {itemsList.map(this._renderItem)}
      </Grid>
    );
  };

  _renderItem = item => {
    const {
      itemId,
      itemData,
      itemContent,
      gridItemWrapClassName,
      itemClassName
    } = item;

    return(
      <Grid
          key={itemId}
          item
          className={
            cn(
              'simple-item-choose-panel__grid-item-wrap',
              gridItemWrapClassName
            )
          }
      >
        <Paper
            className={
              cn(
                'simple-item-choose-panel__item',
                itemClassName
              )
            }
            onClick={this._getOnSelectItemCb(itemId, itemData)}
        >
          <div className="simple-item-choose-panel__item-content-wrap">
            {itemContent}
          </div>
        </Paper>
      </Grid>
    );
  };

  _getOnSelectItemCb = (itemId, itemData) => () => this.props.onSelectItem(itemId, itemData);

  _renderNoItemsInfo = () =>
    <div className="simple-item-choose-panel__no-items-text">
      {this.props.noItemsText}
    </div>;

  render(){
    return(
      <div className="simple-item-choose-panel">
        {
          !!this.props.itemsList.length ?
            this._renderItems() :
            this._renderNoItemsInfo()
        }
      </div>
    );
  }
}