export const FORMAT_FULL_DATE = 'DD.MM.YYYY';
export const FORMAT_SHORT_TIME = 'DD.MM.YYYY HH:mm';
export const FORMAT_FULL_TIME = 'DD.MM.YYYY HH:mm:ss';