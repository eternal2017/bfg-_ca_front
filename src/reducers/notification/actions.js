import { show, hide, removeAll } from 'react-notification-system-redux';

const DEFAULT_NOTIFICATION_OPTIONS = {
  position: 'br',
  autoDismiss: 5
};

export const sendNotification = (message, level, options) => {
  const notificationOptions = {
    ...DEFAULT_NOTIFICATION_OPTIONS,
    ...options,
    message
  };
  return show(notificationOptions, level);
};

export const clearNotificationsByScope = scope =>
  (dispatch, getState) =>
    getState().notification
      .filter(({scope: notificationScope}) => notificationScope === scope)
      .forEach(({ uid }) => dispatch(hide(uid)));

export const clearAllNotifications = removeAll;