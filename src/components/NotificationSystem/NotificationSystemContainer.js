import { connect } from 'react-redux';
import { NotificationSystem } from './NotificationSystem';
import { notificationsSelector } from '../../reducers/notification/selectors';

const mapStateToProps = state => ({
  notifications: notificationsSelector(state)
});

export const NotificationSystemContainer = connect(mapStateToProps)(NotificationSystem);
NotificationSystemContainer.displayName = 'NotificationSystemContainer';