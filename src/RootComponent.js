import React from 'react';

import { Provider } from 'react-redux';

import { store, history } from './reduxStore';

import { ConnectedRouter } from 'connected-react-router';

import { AppContainer } from './components/App/AppContainer';
import { GlobalAppSpinnerContainer } from './components/GlobalAppSpinner/GlobalAppSpinnerContainer';
import { NotificationSystemContainer } from './components/NotificationSystem/NotificationSystemContainer';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';

import './style.css';

const theme = createMuiTheme({
  palette: {
    primary: blue
  },
  typography: {
    fontFamily: [
      'Roboto Condensed',
      'sans-serif'
    ].join(','),
  }
});


const RootComponent = () =>
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <React.Fragment>
        <MuiThemeProvider theme={theme}>
          <AppContainer />
        </MuiThemeProvider>
        <GlobalAppSpinnerContainer />
        <NotificationSystemContainer />
      </React.Fragment>
    </ConnectedRouter>
  </Provider>;

export default RootComponent;
