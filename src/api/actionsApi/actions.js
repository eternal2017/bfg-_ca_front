import { fetchData, postData } from '../httpRequestsApi/actions';

import humps from 'humps';

const { BACKEND_SERVER_HOST } = window.config;

/*
* Для инциирования различных действий сервера со стороны клиента используется отправка POST и GET запросов на, так называемые,
* action-точки.
* */


export const sendServerAction = (actionPoint, actionData, options = {}) =>
  dispatch => {

    const requestBody = actionData.action ?
      {...actionData, action: humps.decamelize(actionData.action)} :
      actionData;

    return dispatch(postData(
      getActionUrl(actionPoint),
      requestBody,
      options
    ));
  };

const getActionUrl = actionPoint => [
  BACKEND_SERVER_HOST,
  'action',
  actionPoint
].join('/');


export const fetchDataFromServerActionPoint = (actionPoint, queryParams = {}, options = {}) =>
  dispatch =>
    dispatch(fetchData(
      getActionUrl(actionPoint),
      queryParams,
      options
    ));
