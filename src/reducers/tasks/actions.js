import { fetchEntities } from '../../reducers/entities/actions';

import { tasksDataSelector } from './selectors';

import {
  DEPARTMENT_MODEL,
  ENTITY_MODEL,
  ENTITY_ROUTE_MODEL,
  EQUIPMENT_CLASS_MODEL,
  EQUIPMENT_MODEL,
  OPERATION_MODEL, ORDER_MODEL,
  SIMULATION_ENTITY_BATCH_MODEL, SIMULATION_EQUIPMENT_MODEL,
  SIMULATION_OPERATION_TASK_EQUIPMENT_MODEL,
  SIMULATION_OPERATION_TASK_MODEL,
  SIMULATION_ORDER_ENTITY_BATCH_MODEL
} from '../../constants/models';
import { FILTER_GROUP_TYPES, FILTER_TYPES, WITH_PARAMS } from '../../api/restCollectionApi';

const { TASKS_PERIOD_TO_SHOW_IN_HOURS } = window.config;

export const SAVE_CURRENT_TASKS_DATA = 'SAVE_CURRENT_TASKS_DATA';

export const saveCurrentTasksData = (
  sessionId,
  departmentId,
  equipmentClassId,
  tasksData
) => ({
  type: SAVE_CURRENT_TASKS_DATA,
  tasksDataUniqId: getTasksDataUniqId(sessionId, departmentId, equipmentClassId),
  tasksData
});

export const getTasksDataUniqId = (sessionId, departmentId, equipmentClassId) =>
  [sessionId, departmentId, equipmentClassId].join('_');


const RESOURCE_TYPE_TASKS_REQUEST_COMMON_WITH_PARAMS = [
  DEPARTMENT_MODEL,
  SIMULATION_OPERATION_TASK_MODEL,
  SIMULATION_ENTITY_BATCH_MODEL,
  OPERATION_MODEL,
  ENTITY_ROUTE_MODEL,
  ENTITY_MODEL,
  {
    column: SIMULATION_ORDER_ENTITY_BATCH_MODEL,
    params: [
      {key: WITH_PARAMS.STRICT, value: false}
    ]
  },
  ORDER_MODEL
];

const EQUIPMENT_TASKS_REQUEST_WITH_PARAMS = [
  {
    column: EQUIPMENT_MODEL,
    params: [
      {key: WITH_PARAMS.STRICT, value: false}
    ]
  },
  EQUIPMENT_CLASS_MODEL,
  SIMULATION_OPERATION_TASK_EQUIPMENT_MODEL
].concat(RESOURCE_TYPE_TASKS_REQUEST_COMMON_WITH_PARAMS);


//Задания будут запрашиваться на этот период
const DEFAULT_TASKS_REQUEST_PERIOD_IN_HOURS = 72;
export const TASKS_REQUEST_PERIOD_IN_HOURS =
  TASKS_PERIOD_TO_SHOW_IN_HOURS ||
  DEFAULT_TASKS_REQUEST_PERIOD_IN_HOURS;
export const TASKS_REQUEST_PERIOD_FILTER = {
  column: 'simulationOperationTask__startTime',
  filterType: FILTER_TYPES.LESS_OR_EQUAL,
  filterValue: TASKS_REQUEST_PERIOD_IN_HOURS
};

const EQUIPMENT_TASKS_REQUEST_MODEL_RELATIONS = {
  equipment: {
    level: 1
  },
  equipmentClass: {
    level: 1
  },
  department: {
    level: 1
  },
  simulationOperationTaskEquipment: {
    level: 1
  },
  simulationOperationTask: {
    level: 2,
    relates: 'simulationOperationTaskEquipment'
  },
  operation: {
    level: 3,
    relates: 'simulationOperationTask'
  },
  simulationEntityBatch: {
    level: 3,
    relates: 'simulationOperationTask'
  },
  entityRoute: {
    level: 4,
    relates: 'operation'
  },
  entity: {
    level: 4,
    relates: 'simulationEntityBatch'
  },
  simulationOrderEntityBatch: {
    level: 4,
    relates: 'simulationEntityBatch'
  },
  order: {
    level: 5,
    relates: 'simulationOrderEntityBatch'
  }
};


export const fetchTasksForEquipmentClassInDepartment = (sessionId, departmentId, equipmentClassId) =>
  (dispatch, getState) => {

    const isTasksDataForCurrentRequestParamsInStore = !!tasksDataSelector(
      getState(),
      { tasksDataUniqId: getTasksDataUniqId(sessionId, departmentId, equipmentClassId) }
    );

    if(isTasksDataForCurrentRequestParamsInStore) return Promise.resolve();

    const queryParams = {
      with: EQUIPMENT_TASKS_REQUEST_WITH_PARAMS,
      filter: {
        filterGroupType: FILTER_GROUP_TYPES.AND,
        filters: [
          {
            column: 'simulationSessionId',
            filterType: FILTER_TYPES.EQUALS,
            filterValue: sessionId
          },
          {
            column: 'equipmentClassId',
            filterType: FILTER_TYPES.EQUALS,
            filterValue: equipmentClassId
          },
          {
            column: 'departmentId',
            filterType: FILTER_TYPES.EQUALS,
            filterValue: departmentId
          },
          TASKS_REQUEST_PERIOD_FILTER
        ]
      },

    };

    const requestOptions = {
      modelRelations: EQUIPMENT_TASKS_REQUEST_MODEL_RELATIONS
    };

    return dispatch(fetchEntities(SIMULATION_EQUIPMENT_MODEL, queryParams, requestOptions))
      .then(({entities}) =>
        dispatch(saveCurrentTasksData(sessionId, departmentId, equipmentClassId, entities)));
  };