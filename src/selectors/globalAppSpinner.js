import { createSelector } from 'reselect';

import { activeBlockingAsyncActionsSelector } from '../reducers/blockingAsyncAction/selectors';

export const showGlobalAppSpinnerSelector = createSelector(
  activeBlockingAsyncActionsSelector,
  activeRequests => !!Object.keys(activeRequests).length
);