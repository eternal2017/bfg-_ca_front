export const SHOW_ADMIN_SETTINGS = 'SHOW_ADMIN_SETTINGS';
export const SET_DEPARTMENT_FOR_EQUIPMENT_CLASS_SELECT_OPTIONS = 'SET_DEPARTMENT_ID_FOR_EQUIPMENT_CLASS_SELECT_OPTIONS';

export const showAdminSettings = settingsId => ({
  type: SHOW_ADMIN_SETTINGS,
  settingsId
});

export const setDepartmentForEquipmentClassSelectOptions = departmentId => ({
  type: SET_DEPARTMENT_FOR_EQUIPMENT_CLASS_SELECT_OPTIONS,
  departmentId
});