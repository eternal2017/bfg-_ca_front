import React, { Component} from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

import { Select } from '../../common/Select/Select';

import {
  CREATE_SETTINGS_USING_SELECTED_AS_TEMPLATE,
  CREATE_NEW_SETTINGS,
  ADMIN_SETTINGS_REACT_SELECTS_OVERRIDE_STYLES
} from '../../../constants/settings';
import { DELETE_LABEL, SETTINGS_LABEL, EDIT_LABEL } from '../../../constants/labels';

import {
  FUNC_IS_REQUIRED_TYPE,
  NUMBER_OR_STRING_TYPE,
  SELECT_OPTIONS_TYPE
} from '../../../constants/propTypes';

import cn from 'classnames';

import './style.css';


export class AdminSettingsControlPanel extends Component {

  static propTypes = {
    isDisabled: PropTypes.bool,
    startNewSettingsCreating: FUNC_IS_REQUIRED_TYPE,
    startNewSettingsCreatingUsingSelectedAsTemplate: FUNC_IS_REQUIRED_TYPE,
    selectedSettingsId: NUMBER_OR_STRING_TYPE.isRequired,
    settingsSelectOptions: SELECT_OPTIONS_TYPE.isRequired,
    selectSettings: FUNC_IS_REQUIRED_TYPE,
    startSettingsEditing: FUNC_IS_REQUIRED_TYPE,
    deleteSelectedSettings: FUNC_IS_REQUIRED_TYPE,
    isDeleteSettingsButtonVisible: PropTypes.bool
  };

  _renderCreateNewSettingsButtons = () => {
    const {
      startNewSettingsCreating,
      startNewSettingsCreatingUsingSelectedAsTemplate
    } = this.props;
    return(
      <div className="admin-settings-control-panel__create-new-settings-buttons">
        <Button onClick={startNewSettingsCreating} variant="contained" color="primary">
          {CREATE_NEW_SETTINGS}
        </Button>
        <Button onClick={startNewSettingsCreatingUsingSelectedAsTemplate} variant="contained">
          {CREATE_SETTINGS_USING_SELECTED_AS_TEMPLATE}
        </Button>
      </div>
    );
  };

  _renderSettingsSelectTitle = () =>
    <div className="admin-settings-control-panel__settings-select-title">
      {SETTINGS_LABEL}
    </div>;

  _renderSettingsSelect = () => {
    const {
      selectedSettingsId,
      selectSettings,
      settingsSelectOptions
    } = this.props;
    return (
      <Select
          className="admin-settings-control-panel__settings-select"
          selected={selectedSettingsId}
          onChange={selectSettings}
          options={settingsSelectOptions}
          isClearable={false}
          placeHolderText={''}
          reactSelectStyles={ADMIN_SETTINGS_REACT_SELECTS_OVERRIDE_STYLES}
      />
    );
  };

  _renderSettingsSelectControlButtons = () => {
    const {
      startSettingsEditing,
      deleteSelectedSettings,
      isDeleteSettingsButtonVisible
    } = this.props;
    return(
      <React.Fragment>
        <Button onClick={startSettingsEditing} variant="outlined">
          {EDIT_LABEL}
        </Button>
        {
          isDeleteSettingsButtonVisible &&
          <Button onClick={deleteSelectedSettings} variant="outlined" color="secondary">
            {DELETE_LABEL}
          </Button>
        }
      </React.Fragment>
    );
  };

  render() {
    return (
      <div
          className={cn(
            'admin-settings-control-panel',
            {'admin-settings-control-panel--disabled': this.props.isDisabled}
          )}
      >
        {this._renderCreateNewSettingsButtons()}
        {this._renderSettingsSelectTitle()}
        <div className="admin-settings-control-panel__settings-select-with-controls-wrap">
          {this._renderSettingsSelect()}
          {this._renderSettingsSelectControlButtons()}
        </div>
      </div>
    );
  }
}