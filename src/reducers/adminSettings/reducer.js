import { SET_DEPARTMENT_FOR_EQUIPMENT_CLASS_SELECT_OPTIONS, SHOW_ADMIN_SETTINGS } from './actions';

import { MAIN_SETTINGS_ID } from '../../constants/settings';

const ADMIN_SETTINGS_INITIAL_STATE = {
  selectedSettingsId: MAIN_SETTINGS_ID,
  departmentForEquipmentClassSelectOptions: null
};

export const adminSettings = (state = {...ADMIN_SETTINGS_INITIAL_STATE}, action) => {
  switch(action.type){
  case SHOW_ADMIN_SETTINGS:
    return {
      ...state,
      selectedSettingsId: action.settingsId
    };
  case SET_DEPARTMENT_FOR_EQUIPMENT_CLASS_SELECT_OPTIONS:
    return{
      ...state,
      departmentForEquipmentClassSelectOptions: action.departmentId
    };
  default:
    return state;
  }
};