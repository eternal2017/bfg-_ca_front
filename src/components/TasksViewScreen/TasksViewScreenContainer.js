import { compose } from 'redux';
import { connect } from 'react-redux';

import { asyncComponent } from '../../hoc/asyncComponent/asyncComponent';

import { push } from 'connected-react-router'

import { fetchTasksForEquipmentClassInDepartment } from '../../reducers/tasks/actions';

import { initTasksDepartmentFilterFromRoute } from '../../operations/departments';
import { initTasksEquipmentClassFilterFromRoute } from '../../operations/equipmentClasses';
import { fetchSettingsByIds } from '../../operations/settings';

import {
  simulationSessionTasksFilterSelector,
  departmentTasksFilterSelector,
  equipmentClassTasksFilterSelector
} from '../../reducers/tasksMainFilters/selectors';
import {
  currentTasksTableColumnsSelector,
  currentTasksTableDataSelector
} from '../../selectors/taskView';

import { MAIN_SETTINGS_ID } from '../../constants/settings';

import { TasksViewScreen } from './TasksViewScreen';


const mapStateToProps = (state) => {
  const { id: simulationSessionIdTasksFilter } = simulationSessionTasksFilterSelector(state);
  const {
    id: departmentIdTasksFilter,
    identity: departmentIdentityTasksFilter
  } = departmentTasksFilterSelector(state);
  const {
    id: equipmentClassIdTasksFilter,
    identity: equipmentClassIdentityTasksFilter
  } = equipmentClassTasksFilterSelector(state);
  const tasksTableColumns = currentTasksTableColumnsSelector(state);
  const tasksTableData = currentTasksTableDataSelector(state);

  return {
    simulationSessionIdTasksFilter,
    departmentIdTasksFilter,
    departmentIdentityTasksFilter,
    equipmentClassIdTasksFilter,
    equipmentClassIdentityTasksFilter,
    tasksTableColumns,
    tasksTableData
  };
};

const mapDispatchToProps = {
  initTasksDepartmentFilterFromRoute,
  initTasksEquipmentClassFilterFromRoute,
  fetchTasksForEquipmentClassInDepartment,
  push,
  fetchSettingsByIds
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {
    simulationSessionIdTasksFilter,
    departmentIdTasksFilter,
    departmentIdentityTasksFilter,
    equipmentClassIdTasksFilter,
    equipmentClassIdentityTasksFilter,
    tasksTableColumns,
    tasksTableData
  } = stateProps;
  const {
    initTasksDepartmentFilterFromRoute,
    initTasksEquipmentClassFilterFromRoute,
    fetchTasksForEquipmentClassInDepartment,
    push,
    fetchSettingsByIds
  } = dispatchProps;

  return {
    simulationSessionIdTasksFilter,
    departmentIdTasksFilter,
    equipmentClassIdTasksFilter,
    tasksTableColumns,
    tasksTableData,
    fetchRequiredData: () => {

      //Для таблицы нужно запросить настройки - общие, настройки для подразделения и настройка для класса РЦ в подразделении
      const fetchRequiredSettingsCb = (departmentIdentityTasksFilter, equipmentClassIdentityTasksFilter) => {
        const equipmentClassInDepartmentSettingsId = [
          departmentIdentityTasksFilter,
          equipmentClassIdentityTasksFilter
        ].join('_');
        //проверка на то, что настройки с такими идентфиикаторами уже запрашивались производится в fetchSettingsByIds
        return fetchSettingsByIds([
          MAIN_SETTINGS_ID,
          departmentIdentityTasksFilter,
          equipmentClassInDepartmentSettingsId
        ]);
      };


      /*
      * Если все фильтры для заданий установлены в state, это значит, что никаких дополнительных проверок выполнять не
      * не нужно. Просто запрашиваем задания для указанных фильтров. simulationSessionIdTasksFilter не проверяем, т.к.
      * если его нет, то этот экран вообще не отрисуется.
      * */
      if(!!departmentIdTasksFilter && !!equipmentClassIdTasksFilter)
        return Promise.all([
          fetchRequiredSettingsCb(departmentIdentityTasksFilter, equipmentClassIdentityTasksFilter),
          fetchTasksForEquipmentClassInDepartment(
            simulationSessionIdTasksFilter,
            departmentIdTasksFilter,
            equipmentClassIdTasksFilter
          )
        ]);

      /*
      * Если хотя бы одного фильтра нет в state, то их нужно инициализировать из параметров роута, если это возможно
      * */
      const {
        departmentIdentity: departmentIdentityFromRoute,
        equipmentClassIdentity: equipmentClassIdentityFromRoute
      } = ownProps.match.params;

      return initTasksDepartmentFilterFromRoute(departmentIdentityFromRoute)
        .then(
          departmentId =>
            Promise.all([
              departmentId,
              initTasksEquipmentClassFilterFromRoute(equipmentClassIdentityFromRoute)
            ])
            .then(
              ([departmentId, equipmentClassId]) => Promise.all([
                fetchRequiredSettingsCb(departmentIdentityFromRoute, equipmentClassIdentityFromRoute),
                fetchTasksForEquipmentClassInDepartment(
                  simulationSessionIdTasksFilter,
                  departmentId,
                  equipmentClassId
                )
              ]),
              /*
              * Если получилось инициализировать подразделение, но не получилось инициализировать класс РЦ, то редиректим
              * на экран выбора класса РЦ для подразделения
              * */
              () => push(`/${departmentIdentityFromRoute}`)
            ),
          () =>  push('/')
        )
    }
  };
};

export const TasksViewScreenContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  asyncComponent({
    resolve: [
      {
        fn: ({fetchRequiredData}) => fetchRequiredData()
      }
    ]
  })
)(TasksViewScreen);

TasksViewScreenContainer.displayName = 'TasksViewScreenContainer';