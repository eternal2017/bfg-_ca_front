/**
 * @function
 * @desc Проверка date на валидность. Возвращает false, если Invalid Date или не объект Date
 *
 * @param d объект  для проверка
 * @returns {boolean} Результат проверки
 */

export function isValidDate(d){
  //не дата - возвращаем сразу false
  if ( Object.prototype.toString.call(d) !== '[object Date]' )  return false;
  return !isNaN( d.getTime() );
}