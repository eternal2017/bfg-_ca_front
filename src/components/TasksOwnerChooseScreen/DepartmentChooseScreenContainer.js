import { compose } from 'redux';
import { connect } from 'react-redux';

import { push } from 'connected-react-router'

import { asyncComponent } from '../../hoc/asyncComponent/asyncComponent';

import { fetchDepartmentsWithTasks } from '../../operations/departments';

import { resetTasksDepartmentFilter, setTasksDepartmentFilter } from '../../reducers/tasksMainFilters/actions';
import { sendNotification, clearNotificationsByScope } from '../../reducers/notification/actions';

import { departmentsListSortedByIdentitySelector } from '../../selectors/department';

import { ONLY_DEPARTMENTS_WITH_TASKS_DISPLAYED_MSG } from '../../messages/department';
import { DEPARTMENT_CHOOSE_SCREEN_NOTIFICATION_SCOPE, NOTIFICATION_LEVEL } from '../../constants/notification';

import { NO_DEPARTMENTS_WITH_TASKS_MSG } from '../../messages/department';

import { TasksOwnerChooseScreen } from './TasksOwnerChooseScreen';

const mapStateToProps = state => {
  return {
    departmentsList: departmentsListSortedByIdentitySelector(state)
  };
};

const mapDispatchToProps = {
  fetchDepartmentsWithTasks,
  push,
  setTasksDepartmentFilter,
  resetTasksDepartmentFilter,
  sendNotification,
  clearNotificationsByScope
};

const mergeProps = (stateProps, dispatchProps) => {
  const { departmentsList } = stateProps;
  const {
    fetchDepartmentsWithTasks,
    push,
    setTasksDepartmentFilter,
    resetTasksDepartmentFilter,
    sendNotification,
    clearNotificationsByScope
  } = dispatchProps;
  return {
    tasksOwnersList: departmentsList
      .map(departmentEntity => {
        const {
          id,
          name: ownerMainTitle,
          identity: ownerSecondaryTitle
        } = departmentEntity;
        return {
          id,
          ownerMainTitle,
          ownerSecondaryTitle,
          ownerOriginalData: departmentEntity
        }
      }),
    resetTasksOwnerInStore: resetTasksDepartmentFilter,
    onSelectTasksOwner: (_, departmentEntity) => {
      setTasksDepartmentFilter(departmentEntity);
      push(`/${departmentEntity.identity}`);
    },
    emptyOwnerListMessage: NO_DEPARTMENTS_WITH_TASKS_MSG,
    showDisplayedTasksOwnersInfoNotification: () =>
      sendNotification(
        ONLY_DEPARTMENTS_WITH_TASKS_DISPLAYED_MSG,
        NOTIFICATION_LEVEL.INFO,
        { autoDismiss: 0, scope: DEPARTMENT_CHOOSE_SCREEN_NOTIFICATION_SCOPE }
      ),
    clearDisplayedTasksOwnersInfoNotification: () =>
      clearNotificationsByScope(DEPARTMENT_CHOOSE_SCREEN_NOTIFICATION_SCOPE),
    fetchDepartmentsWithTasks: () => {
      if(!!departmentsList.length) return Promise.resolve();
      return fetchDepartmentsWithTasks();
    },
  };
};

export const DepartmentChooseScreenContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  asyncComponent({
    resolve: [
      {
        fn: ({fetchDepartmentsWithTasks}) => fetchDepartmentsWithTasks()
      }
    ]
  })
) (TasksOwnerChooseScreen);

DepartmentChooseScreenContainer.displayName = 'DepartmentChooseScreenContainer';