import { Settings } from './Settings';

import { SETTINGS_MODEL } from '../../constants/models';

import _size from 'lodash/size';
import _partial from 'lodash/partial';


const ENTITIES_TRANSFORMERS_BY_MODEL_MAP = {
  [SETTINGS_MODEL]: Settings
};

/*
* Подразумевается, что все трансформеры наследуются от абстрактного класса Transformer, у которого есть реализация
* по умолчанюи для методов transformToState и transformToDb, поэтому дополнительных проверок на существование
* этих функций не делается, а экспортируемые функции transformEntitiesToState и transformEntitiesToDb вызывают
* функцию transform с этими значениями.
* */
const transform = (transformFuncIdentity, modelEntitiesArray, stateModel, state) => {
  if(!_size(modelEntitiesArray)) return [];

  const EntityTransformer = ENTITIES_TRANSFORMERS_BY_MODEL_MAP[stateModel];

  if(!EntityTransformer) return modelEntitiesArray;

  return modelEntitiesArray.map(entity => EntityTransformer[transformFuncIdentity](entity, state));
};

export const transformEntitiesToState = _partial(transform, 'transformToState');

export const transformEntitiesToDb = _partial(transform, 'transformToDb');