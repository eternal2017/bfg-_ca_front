import { fetchEntities, fetchEntitiesAndAddToStore } from '../reducers/entities/actions';
import { setTasksDepartmentFilter } from '../reducers/tasksMainFilters/actions';

import { simulationSessionTasksFilterSelector } from '../reducers/tasksMainFilters/selectors';

import {
  DEPARTMENT_MODEL,
  SIMULATION_EQUIPMENT_MODEL,
  SIMULATION_OPERATION_TASK_EQUIPMENT_MODEL, SIMULATION_OPERATION_TASK_MODEL
} from '../constants/models';
import { FILTER_GROUP_TYPES, FILTER_TYPES } from '../api/restCollectionApi';
import { TASKS_REQUEST_PERIOD_FILTER } from '../reducers/tasks/actions';

export const fetchDepartments = (queryParams = {}, requestOptions = {}) =>
  dispatch => dispatch(fetchEntitiesAndAddToStore(DEPARTMENT_MODEL, queryParams, requestOptions));


/*
* Запрос только тех подразделений, для которых есть задания в отображаемый период
* */
export const DEPARTMENTS_WITH_TASKS_REQUEST_MODEL_RELATIONS = {
  [SIMULATION_EQUIPMENT_MODEL]: {
    level: 1
  },
  [SIMULATION_OPERATION_TASK_EQUIPMENT_MODEL]: {
    level: 2,
    relates: SIMULATION_EQUIPMENT_MODEL
  },
  [SIMULATION_OPERATION_TASK_MODEL]: {
    level: 3,
    relates: SIMULATION_OPERATION_TASK_EQUIPMENT_MODEL
  }
};
export const fetchDepartmentsWithTasks = () =>
  (dispatch, getState) => {
    const { id: simulationSessionIdTasksFilter } = simulationSessionTasksFilterSelector(getState());

    if(!simulationSessionIdTasksFilter) return Promise.resolve();

    const queryParams = {
      filter: {
        filterGroupType: FILTER_GROUP_TYPES.AND,
        filters: [
          {
            column: [SIMULATION_EQUIPMENT_MODEL, 'simulationSessionId'].join('__'),
            filterType: FILTER_TYPES.EQUALS,
            filterValue: simulationSessionIdTasksFilter
          },
          TASKS_REQUEST_PERIOD_FILTER
        ]
      }
    };

    const requestOptions = {
      modelRelations: {...DEPARTMENTS_WITH_TASKS_REQUEST_MODEL_RELATIONS}
    };

    return dispatch(fetchDepartments(queryParams, requestOptions));
  };


/*
* В роутинге устанавливаются фильтр заданий по подразделению в виде его клиентского идентификатора. В url,
* теоретически, может быть написано всё что угодно + данные для которые устанавливался ранее фильтр могут быть изменены.
* Поэтому в определенных случаях необходимо выполнить проверку, есть ли подразделение с таким клиентским идентификатором
* в БД, чтобы корректно продолжить работу.
*
* Если такое подразделение существует, то устанавливаем фильтр в state. Если нет, то реджектим.
* */
export const initTasksDepartmentFilterFromRoute = departmentIdentityFromRoute =>
  dispatch => {

    const departmentCheckQueryParams = {
      filter: {
        filterGroupType: FILTER_GROUP_TYPES.AND,
        filters: [{
          column: 'identity',
          filterType: FILTER_TYPES.EQUALS,
          filterValue: departmentIdentityFromRoute
        }]
      }
    };

    return dispatch(fetchEntities(DEPARTMENT_MODEL, departmentCheckQueryParams, {isBlockingRequest: false}))
      .then(({ entities, responseEntitiesIds, responseMeta }) => {

        if (responseMeta.count === 0)
          return Promise.reject(departmentIdentityFromRoute);

        const departmentId = responseEntitiesIds[DEPARTMENT_MODEL][0];
        /*
        * Подразделение существует, устанавливаем фильтр в state
        */
        dispatch(setTasksDepartmentFilter(entities[DEPARTMENT_MODEL][departmentId]));
        return departmentId
      })
  };