import { isValidDate } from './date';

/**
 * @function
 * @desc Расширенный компаратор строк, игнорирующий регистр и обрабатывающий помимо обычной строковой сортировки
 * случаи с falsy значениями (undefined, null и пустую строку), которые сортируются в конец списка, при сортировке по
 * возрастанию
 *
 * @param a Первый элемент
 * @param b Второй элемент
 * @returns {number} Результат сравнения
 */
export const stringComparator = (a, b) => {
  if(!a && !b) return 0;
  if(!a && b) return 1;
  if(!b && a) return -1;
  if(a.toUpperCase() > b.toUpperCase()) return 1;
  if(a.toUpperCase() < b.toUpperCase()) return -1;
};


export const numberComparator = (a, b) => a - b;


/**
 * @function
 * @desc Фабрика для создания компараторов, которая применяет, передаваемый в параметре компаратор не на
 * самих элементах, а на их свойствах. Таким образом, предполагается, что элементы, передаваемые в созданный
 * таким образом компаратор, являются объектами.
 *
 * @param comparator Компаратор для сравнения свойств объектов
 * @param field Свойство элемента, по которому производится сравнение объектов.
 * @returns {Function} Компаратор cmp, сравнивающий свойства элементов.
 */
export const fieldComparatorFactory = (comparator, field) =>
  (a, b) => comparator(a[field], b[field]);


/**
 * @function
 * @desc Компаратор дат. Перемещает элементы, которые не являются датами в конец списка
 *
 * @param a Первый элемент
 * @param b Второй элемент
 * @returns {number} Результат сравнения
 */
export const dateComparator = (a, b) => {
  const aDate = new Date(a);
  const bDate = new Date(b);
  const aValid = isValidDate(aDate);
  const bValid = isValidDate(bDate);
  if(!aValid && !bValid) return 0;
  if(!aValid && bValid) return 1;
  if(!bValid && aValid) return -1;
  return aDate.getTime() - bDate.getTime();
};


/**
 * @function
 * @desc Фабрика компаратора, который в качестве критерия сравнения
 * использует индекс элемента в массиве.
 *
 * @param array Оригинальный массив, по расположению в котором принимается
 * решение о результате сравнения.
 * @returns {Function} Компаратор
 */
export const indexComparatorFactory = array =>
  (a, b) => numberComparator(array.indexOf(a) - array.indexOf(b));


/**
 * @function
 * @desc Фабрика сложного компаратора. Возвращает компаратор, который производит сравнение посредством применения
 * одного или нескольких компараторов, в той последовательности, в которой они объявлены при инициализации фабрики.
 * Компараторы применяются до тех пор, пока один из них не вернет значение больше (1) или меньше (-1), вплоть до последнего.
 *
 * @param comparators Компараторы, которые участвуют в сложном компараторе.
 * @returns {Function} Компаратор
 */
export const complexComparatorFactory = (...comparators) =>
  (a, b) => {
    let compareResult = 0;
    /*
    * Как только один из компараторов по порядку выдал 1 и -1 (=true, some заканчивает работу), то цикл сразу бы
    * остановился, т.к. результат комбинированного компаратора уже получен. Если же текущий компаратор выдает 0 (=false,
    * some продолжает перебор), то это значит, что значения по текущему компаратору равны и нужно проверять по-следующему
    * компаратору. Если все компараторы выдали 0, то значит значения равны по всем критериям сравнения
    * */
    comparators.some(cmp => compareResult = cmp(a, b));
    return compareResult;
  };


//Переворачивает значение компаратора
export const negateComparator = comparator => (...args) => -1 * comparator(...args);