export const NO_DEPARTMENTS_WITH_TASKS_MSG = 'На текущий момент в системе нет заданий ни для одного из подразделений';

export const ONLY_DEPARTMENTS_WITH_TASKS_DISPLAYED_MSG =
  'В системе не отображаются подразделения, для которых не были сформированы задания';