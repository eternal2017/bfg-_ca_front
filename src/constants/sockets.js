/*
* Сокет для общесистемных сообщений
* */
export const SYSTEM_MESSAGES_SOCKET_IDENTITY = 'message';


/*
* Тип клиентских сообщений в сокет message (сообщений броадкастинга)
* */
export const CLIENT_SOCKET_MESSAGE_TYPE = 'CLIENT_MESSAGE_CUSTOM_EVENTS';

/*
* В клиентских соощениях в сокете в зависимости от размера пересылаемых полезных данных, эти данные могут записываться
* или в самом теле сообщения или загружаться на эту временную точку
* */
export const CLIENT_SOCKET_MESSAGE_TEMPORARY_DATA_POINT = 'temporary';


/*
* Тип события по смене сесии, для которой отобрадаются задания в приложении
* */
export const SIMULATION_SESSION_FOR_TASKS_CHANGED_EVENT_TYPE = 'ACTIVE_QRM_SESSION_CHANGED';