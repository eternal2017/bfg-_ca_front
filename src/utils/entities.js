import _mapValues from 'lodash/mapValues';

/**
 * Часто нужно на основании объекта данных получить новый объект, в который нужно только часть его данных в
 * определенные ключи по шаблону
 * @param entity - исходный объект
 * @param template - карта, где ключи это наименования полей результирующего объекта, а значения - это ключи исходного
 * объекта, по которым нужно взять значения
 * return {Object}
 */
export const getEntityDataByTemplate = (entity, template) => {
  if(!entity || !template) return {};
  return _mapValues(
    template,
    entityKey => entity[entityKey]
  )
};