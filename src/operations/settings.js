import {
  fetchEntitiesAndAddToStore,
  saveEntitiesAndAddToStore,
  deleteEntitiesFromStore,
  deleteEntities
} from '../reducers/entities/actions';
import { entitySelector } from '../reducers/entities/selectors';

import { SETTINGS_MODEL} from '../constants/models';
import { FILTER_GROUP_TYPES, FILTER_TYPES } from '../api/restCollectionApi';
import { CA_FRONTEND_SETTINGS_GROUP } from '../constants/settings';

import _size from 'lodash/size';
import { adminSettingsSelectOptionsSelector } from '../selectors/adminSettings';
import { showAdminSettings } from '../reducers/adminSettings/actions';

/*
* Экшен криеторы для работы с моделью БД settings. Особенность модели в том, что данные в ней хранятся в поле
* JSON и, фактически, здесь может храниться любая нужная информация, без создания дополнительных моделей.
*
* Все настройки для приложения ca_frontend будут храниться в служебной группе CA_FRONTEND_SETTINGS_GROUP, чтобы они
* были уникальны, не пересекались с другими веб-приложениями, использующими модель settings. Т.е. признак уникальности
* настроек у ca_frontend, это имя (поле name). Это также учитывается в трансформере transformers/entities/Settings.js
* */

export const fetchSettings = filters =>
  dispatch => {

    const appSettingsGroupFilter = {
      column: 'group',
      filterType: FILTER_TYPES.EQUALS,
      filterValue: CA_FRONTEND_SETTINGS_GROUP
    };

    const resultFilters = !!_size(filters) ?
      [appSettingsGroupFilter].concat(filters) :
      [appSettingsGroupFilter];

    const queryParams = {
      filter: {
        filterGroupType: FILTER_GROUP_TYPES.AND,
        filters: resultFilters
      }
    };

    return dispatch(fetchEntitiesAndAddToStore(SETTINGS_MODEL, queryParams));
  };

export const fetchSettingsByIds = settingsIds =>
  (dispatch, getState) => {

    /*
     * Если не указан массив идентификаторов, или сущности по таким идентификатором уже былаи ранее получены,
     * не делаем запрос
     * */
    if(!_size(settingsIds)) return Promise.resolve();

    const state = getState();

    const settingsIdsToFetch = settingsIds
      .filter(settingsId => !entitySelector(state, { model: SETTINGS_MODEL, id: settingsId }));

    if(!_size(settingsIdsToFetch)) return Promise.resolve();

    const settingsFetchFilters = {
      filterGroupType: FILTER_GROUP_TYPES.OR,
      filters: settingsIdsToFetch
        .map(settingsId => ({
          column: 'name',
          filterType: FILTER_TYPES.EQUALS,
          filterValue: settingsId
        }))
    };
    return dispatch(fetchSettings([settingsFetchFilters]));
  };

export const saveSettingsEntity = (settingsId, settingsData) =>
  (dispatch, getState) => {

    const isSettingNew = !entitySelector(getState(), { model: SETTINGS_MODEL, id: settingsId });

    return dispatch(saveEntitiesAndAddToStore(
      SETTINGS_MODEL,
      [
        {
          id: settingsId,
          data: settingsData
        }
      ],
      isSettingNew
    ))
    /*
      * saveEntitiesAndAddToStore возвращает нормализованные данные. Сохраняем всегда одни настройки. поэтому в
      * единственно значение - это сущность сохраненных настроек
      * */
      .then(normalizedEntities => Object.values(normalizedEntities)[0]);
  };


/*
* Экшн для удаления выбранных в селекте настроек на экране администратора, для него есть следующие особенности:
*  - нужно удалить настройки с сервера использовав реальный id в БД с предопределенным "префиксом" группы настроек
*  приложения (подробнее про группу настроек в комментарии в начале этого файла).
*  - после удаление настроек с сервера нужно удалить настройки из store, чтобы опции селекта обновились. Настройки
*  в store уже хранятся с простым идентификатором настроек без префикса, про который описывалось в предыдущем пункте
*  - также, после удаления нужно заселектить предыдущую по порядку опцию настроек в списке опций
*  - любые удаляемые настройки всегда есть в списке опций селекта, т.е. по id настроек всегда можно найти индекс опции
*  удаляемых настроек, дополнительных проверок на это не нужно.
*  - настройки с id MAIN_SETTINGS_ID запрещается удалять из приложения, это общие настройки и они должны быть всегда в
*  системе (в интерфейсе вообще нет возможности их удалить)
*  - учитывая прошлый пункт, очевидно, что всегда можно найти предыдущая опцию настроек в списке (которую нужно заселектить),
*  относительно удаляемой, т.е. на индекс предыдущей по порядку опции тоже не нужно делать дополнительных проверок
*  *
* */
export const deleteSettings = settingsId =>
  (dispatch, getState) => {

    const settingsServerId = [CA_FRONTEND_SETTINGS_GROUP, settingsId].join('/');
    return dispatch(deleteEntities(SETTINGS_MODEL, [settingsServerId]))
      .then(() => {
        const settingsSelectOptions = adminSettingsSelectOptionsSelector(getState());

        const deletedOptionIndex = settingsSelectOptions
        //eslint-disable-next-line
          .findIndex(({optionPayload}) => settingsId == optionPayload.settingsId);

        const newSelectedSettingsId = settingsSelectOptions[deletedOptionIndex - 1].optionPayload.settingsId;

        dispatch([
          deleteEntitiesFromStore(SETTINGS_MODEL, [settingsId]),
          showAdminSettings(newSelectedSettingsId)
        ]);
      });
  };