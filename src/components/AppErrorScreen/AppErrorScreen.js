import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

export const AppErrorScreen = ({appErrorMsg}) =>
  <div className="cyber-assistant-app-error-screen">
    {appErrorMsg}
  </div>;

AppErrorScreen.propTypes = {
  appErrorMsg: PropTypes.string.isRequired
};