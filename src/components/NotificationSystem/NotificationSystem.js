import React from 'react';
import PropTypes from 'prop-types';

import BaseNotificationSystem from 'react-notification-system-redux';

const defaultStyle = {
  NotificationItem: {
    DefaultStyle: {
      paddingTop: '1.4rem', //иначе по умолчанию текст может немного наезжать на крестик закрытия notification
      fontSize: '1.5rem', //немного увеличиваем шрифт, т.к. по дефолту маловат
      fontFamily: 'Roboto condensed'
    }
  }
};

export const NotificationSystem = ({ notifications }) =>
  <BaseNotificationSystem
      notifications={notifications}
      style={defaultStyle}
  />;


NotificationSystem.propTypes = {
  notifications: PropTypes.arrayOf(PropTypes.any)
};