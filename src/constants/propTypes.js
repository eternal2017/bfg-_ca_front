import PropTypes from 'prop-types';
import { TASK_TYPES } from './tasks';

const {
  string,
  objectOf,
  number,
  arrayOf,
  any,
  oneOfType,
  func,
  shape,
  bool
} = PropTypes;

export const NUMBER_OR_STRING_TYPE = oneOfType([string, number]);

export const OBJECT_OF_ANY_TYPE = objectOf(any);
export const ARRAY_OF_ANY_OBJECTS_TYPE = arrayOf(OBJECT_OF_ANY_TYPE);

export const FUNC_IS_REQUIRED_TYPE = func.isRequired;

export const SELECT_OPTIONS_TYPE = arrayOf(shape({
  optionValue: string.isRequired,
  optionLabel: string.isRequired,
  optionPayload: OBJECT_OF_ANY_TYPE
}));

export const TASKS_TYPES_SETTINGS_TYPE = shape({
  [TASK_TYPES.OPERATION]: bool.isRequired,
  [TASK_TYPES.PREPARING]: bool.isRequired,
  [TASK_TYPES.SETUP]: bool.isRequired,
});

export const SELECTED_ADMIN_SETTINGS_DATA_TYPE = shape({
  departmentIdentity: string,
  departmentName: string,
  equipmentClassIdentity: string,
  equipmentClassName: string,
  tasksTypes: objectOf(bool).isRequired,
  tasksTableColumns: OBJECT_OF_ANY_TYPE.isRequired
});