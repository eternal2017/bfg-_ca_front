import { fetchDataFromServerActionPoint, sendServerAction } from '../api/actionsApi/actions';
import { setAppAuthSessionId, setAppStatus } from '../reducers/appState/actions';
import { connectToSystemMessagesSocket } from '../api/socketApi/actions';
import { fetchEntities } from '../reducers/entities/actions';
import { setTasksSimulationSessionFilter } from '../reducers/tasksMainFilters/actions';

import { APP_STATUS } from '../reducers/appState/reducer';
import { SERVER_ACTION_POINT, SERVER_ACTION_COMMAND } from '../constants/serverActions';
import { SIMULATION_SESSION_MODEL } from '../constants/models';
import { FILTER_GROUP_TYPES, FILTER_TYPES } from '../api/restCollectionApi';

import { closeAllSocketConnections } from '../api/socketApi';

import { getUniqId } from '../utils/getUniqId';

const { APP_LOGIN_NAME, APP_LOGIN_PASSWORD } = window.config;

/*
* При инициализации приложения выполняются необходимые запросы на авторизацию, подключение к сокетам, проверка активного
* процесса импорта, запрос сессии для дальнейшего запроса заданий по ней. Если всё успешно, то считается, что
* приложение успешно проиницилизировано. Если были какие-то, то для приложения устанавливаются ошибочные статусы.
* Установка статусов осуществляется внутри самих внутренних экшенов
* */
export const initApp = () =>
  dispatch =>
    dispatch(logIn())
      .then(() => {
        dispatch(connectToSystemMessagesSocket());

        /*
        * После логина проверяем на активный импорт, и если его нет, то запрашиваем сессию для заданий и устанавливаем
        * её в фильтр. Если идёт импорт или сессию в системе не существует, то устанавливаем соотвествующий статус
        * в системе
        * */

        return dispatch(checkIfNoActiveDataImportProcessExists());
      })
      .then(() => dispatch(fetchSimulationSessionForTasks()));

/*
* Для приложения пока не требуется различать пользователя, поэтому авторизация осуществляется подобно авториазации
* сторонних сервисов системы. Сервисы определены при деплое бэкенда, а данные, нужные для авторизации, передаются через
* переменные окружения. Для этого случая, параметр service должен быть определен как true
* */
export const logIn = () =>
  dispatch => {
    const loginData = {
      login: APP_LOGIN_NAME,
      password: APP_LOGIN_PASSWORD,
      service: true
    };
    return dispatch(sendServerAction(
      SERVER_ACTION_POINT.LOGIN,
      { action: SERVER_ACTION_COMMAND.LOGIN, data: loginData },
      { isBlockingRequest: false, showServerError: false }
    ))
      .then(() => {
        /*
        * Идентификатор сессии авторизации используется как React key для главного компонента обертки над всеми
        * роутами приложения, чтобы когда авторизационная сессия истекла и мы сделали повторную автоматическую
        * авторизацию, ключ изменился и компоненты были замаунчены заново и нужные данные были бы перезапрошены.
        * Подробнее в App.js в методе рендера роутов _renderWorkSpaceContentRoutes()
        * */
        const authSessionUniqId = getUniqId();
        dispatch(setAppAuthSessionId(authSessionUniqId));
      })
      .catch(() => {
        closeAllSocketConnections();
        /*
        * Авторизация может закончится с ошибкой для сервиса только если приложение неверно сконфигурировано или
        * нет соединения с сервером. Для этих случаев устанавливаем соответствующий статус приложению, чтобы на экране
        * пользователю отобразить информацию об этой ошибке
        * */
        dispatch(setAppStatus(APP_STATUS.NOT_INITIALIZED));
        return Promise.reject(APP_STATUS.NOT_INITIALIZED);
      });
  };

/*
* Если при инициализации приложения идёт процесс импорта новых данных, то данные из БД ещё могут быть не
* удалены и сессию для заданий мы получим, но, когда импорт закончится, то всё это сразу же удалится, так что
* если идёт активный импорт, то сразу ничего не делаем, будем просто ожидать сообщение в сокет о том, что принята
* новая сессия для заданий
* */
const checkIfNoActiveDataImportProcessExists = () =>
  dispatch =>
    dispatch(fetchDataFromServerActionPoint(
      SERVER_ACTION_POINT.IMPORT,
      {},
      { isBlockingRequest: false, showServerError: false }
    ))
      .then(
        () => {
          /*
          * Успешный ответ на запрос, означает что сейчас импортируются данные. Как было описано выше, в этом случае
          * сразу устанавливаем нужный статус приложению и реджектим, т.к. ожидается резолв из экшена, когда
          * активного процесса импорта нет
          * */
          dispatch(setAppStatus(APP_STATUS.NO_TASKS_IN_SYSTEM));
          return Promise.reject(APP_STATUS.NO_TASKS_IN_SYSTEM);
        },
        () => {
          /*
          * Если активного процесса импорта нет, то в ответ на запрос получим 400 ошибку, вэтом случае, ожидается
          * резолв их экшена, чтобы можно было продолжить работу. Поэтому нужно определить какой-то любой обработчик,
          * чтобы не происходил реджект
          * */
          return Promise.resolve();
        }
      );

export const fetchSimulationSessionForTasks = () =>
  dispatch =>
    dispatch(fetchDataFromServerActionPoint(
      SERVER_ACTION_POINT.SIMULATION_SESSION_FOR_TASKS,
      {},
      { isBlockingRequest: false }
    ))
      .then(({data: simulationSessionId}) => {
        if(simulationSessionId === null)
          return Promise.reject(APP_STATUS.NO_TASKS_IN_SYSTEM);

        return dispatch(fetchEntities(
          SIMULATION_SESSION_MODEL,
          {
            filter: {
              filterGroupType: FILTER_GROUP_TYPES.AND,
              filters: [
                {
                  column: 'id',
                  filterType: FILTER_TYPES.EQUALS,
                  filterValue: simulationSessionId
                }
              ]
            }
          },
          { isBlockingRequest: false }
        ))
      })
      .then(({entities, responseEntitiesIds, responseMeta}) => {
        if (responseMeta.count === 0)
          return Promise.reject(APP_STATUS.NO_TASKS_IN_SYSTEM);

        const simulationSessionId = responseEntitiesIds[SIMULATION_SESSION_MODEL][0];
        const simulationSessionEntity = entities[SIMULATION_SESSION_MODEL][simulationSessionId];

        dispatch([
          setTasksSimulationSessionFilter(simulationSessionEntity),
          setAppStatus(APP_STATUS.READY_TO_WORK)
        ]);
        return simulationSessionEntity;
      })
      .catch(() => {
        dispatch(setAppStatus(APP_STATUS.NO_TASKS_IN_SYSTEM));
        return Promise.reject(APP_STATUS.NO_TASKS_IN_SYSTEM)
      });