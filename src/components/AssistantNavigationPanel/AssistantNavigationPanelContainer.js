import { connect } from 'react-redux';

import { push } from 'connected-react-router';

import {
  simulationSessionTasksFilterSelector,
  departmentTasksFilterSelector,
  departmentTasksFilterTitleSelector,
  equipmentClassTasksFilterSelector,
  equipmentClassTasksFilterTitleSelector
} from '../../reducers/tasksMainFilters/selectors';
import { appErrorTitleSelector } from '../../reducers/appState/selectors';

import { AssistantNavigationPanel } from './AssistantNavigationPanel';


const mapStateToProps = state => {
  const {
    startDate: tasksPeriodStartDate
  } = simulationSessionTasksFilterSelector(state);
  const {
    identity: departmentIdentityTasksFilter
  } = departmentTasksFilterSelector(state);
  const {
    identity: equipmentClassIdentityTasksFilter
  } = equipmentClassTasksFilterSelector(state);
  return {
    tasksPeriodStartDate,
    departmentIdentityTasksFilter,
    equipmentClassIdentityTasksFilter,
    appErrorTitle: appErrorTitleSelector(state),
    departmentTitle: departmentTasksFilterTitleSelector(state),
    equipmentClassTitle: equipmentClassTasksFilterTitleSelector(state)
  };
};

const mapDispatchToProps = {
  push
};

const mergeProps = (stateProps, dispatchProps) => {
  const {
    tasksPeriodStartDate,
    departmentIdentityTasksFilter,
    equipmentClassIdentityTasksFilter,
    appErrorTitle,
    departmentTitle,
    equipmentClassTitle
  } = stateProps;
  const { push } = dispatchProps;
  const onGoBackButtonClick = !appErrorTitle && departmentIdentityTasksFilter ?
    () => {
      if (!equipmentClassIdentityTasksFilter) return push('/');
      return push(`/${departmentIdentityTasksFilter}`);
    } :
    undefined;
  return {
    appErrorTitle,
    tasksPeriodStartDate,
    departmentTitle,
    equipmentClassTitle,
    onGoBackButtonClick
  };
};

export const AssistantNavigationPanelContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AssistantNavigationPanel);

AssistantNavigationPanelContainer.displayName =
  'AssistantNavigationPanelContainer';
