import { createSelector } from 'reselect';

import { entitiesForModelSelectorFactory } from '../reducers/entities/selectors';

import { equipmentClassesInDepartmentEntitiesSelector } from '../reducers/equipmentClassesInDepartment/selectors';

import { fieldComparatorFactory, stringComparator } from '../utils/arrayComparators';

import { EQUIPMENT_CLASS_MODEL } from '../constants/models';


export const equipmentClassesEntitiesSelector = entitiesForModelSelectorFactory(EQUIPMENT_CLASS_MODEL);

export const equipmentClassesInDepartmentListSortedByIdentitySelector = createSelector(
  equipmentClassesInDepartmentEntitiesSelector,
  entities => Object
    .values(entities || {})
    .sort(fieldComparatorFactory(stringComparator, 'identity'))
);