import { CLEAR_ALL_EQUIPMENT_CLASSES_IN_DEPARTMENT_DATA, SAVE_EQUIPMENT_CLASSES_IN_DEPARTMENT } from './actions';

export const equipmentClassesInDepartment = (state = {}, action) => {
  switch(action.type){
  case SAVE_EQUIPMENT_CLASSES_IN_DEPARTMENT: {
    const { departmentId, equipmentClassEntities } = action;
    return {
      ...state,
      [departmentId]: {...equipmentClassEntities}
    }
  }
  case CLEAR_ALL_EQUIPMENT_CLASSES_IN_DEPARTMENT_DATA:
    return {};
  default:
    return state;
  }
};