import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import { TASK_TYPES, TASK_TYPES_TITLES_MAP } from '../../../../constants/tasks';
import { TASKS_TYPES_SETTINGS_GROUP_TITLE } from '../../../../constants/settings';
import { TASKS_TYPES_SETTINGS_TYPE, FUNC_IS_REQUIRED_TYPE } from '../../../../constants/propTypes';

import cn from 'classnames';

import './style.css';

export class TasksTypesSettingsGroup extends Component {

  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
    tasksTypesSettings: TASKS_TYPES_SETTINGS_TYPE.isRequired,
    editTasksTypesSettings: FUNC_IS_REQUIRED_TYPE
  };

  _renderTasksTypeCheckBox = taskTypeIdentity => {
    const {
      isReadOnly,
      tasksTypesSettings
    } = this.props;
    return(
      <FormControlLabel
          key={taskTypeIdentity}
          className={
            cn(
              'tasks-types-settings-group__tasks-type-checkbox-form-control',
              {'tasks-types-settings-group__tasks-type-checkbox-form-control--disabled': isReadOnly}
            )
          }
          classes={{
            label: 'tasks-types-settings-group__tasks-type-checkbox-label'
          }}
          control={
            <Checkbox
                className="tasks-types-settings-group__tasks-type-checkbox"
                checked={tasksTypesSettings[taskTypeIdentity]}
                onChange={
                  isReadOnly ?
                    undefined :
                    this._tasksTypeCheckboxChangeCbFactory(taskTypeIdentity)
                }
                color="default"
                value={String(taskTypeIdentity)}
                disableRipple={isReadOnly}
            />
          }
          label={TASK_TYPES_TITLES_MAP[taskTypeIdentity]}
      />
    );
  };

  _tasksTypeCheckboxChangeCbFactory = taskTypeIdentity => e => {
    const {
      tasksTypesSettings,
      editTasksTypesSettings
    } = this.props;

    const checked = e.target.checked;
    /*
    * Если пытаемся отключить последний, то ничего не делаем, должен быть выделен хотя бы один тип
    * */
    if(
      !checked &&
      Object
        .values(this.props.tasksTypesSettings)
        .filter(isTaskTypeChecked => isTaskTypeChecked)
        .length === 1
    ) return;
    const newTasksTypesSettings = {
      ...tasksTypesSettings,
      [taskTypeIdentity]: checked
    };
    editTasksTypesSettings(newTasksTypesSettings);
  };

  render() {
    const {
      OPERATION: operationTaskType,
      PREPARING: preparingTaskType,
      SETUP: setupTaskType
    } = TASK_TYPES;
    return(
      <div className="tasks-types-settings-group">
        <div className="tasks-types-settings-group__title">
          {TASKS_TYPES_SETTINGS_GROUP_TITLE}
        </div>
        <FormGroup row className="tasks-types-settings-group__checkboxes-row">
          {
            [operationTaskType, preparingTaskType, setupTaskType].map(this._renderTasksTypeCheckBox)
          }
        </FormGroup>
      </div>
    );
  }
}