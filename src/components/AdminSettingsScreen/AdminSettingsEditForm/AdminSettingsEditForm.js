import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { NewSettingsOwnerSelects } from './NewSettingsOwnerSelects/NewSettingsOwnerSelects';
import { TasksTypesSettingsGroup } from './TasksTypesSettingsGroup/TasksTypesSettingsGroup';
import { TasksTableColumnsSettingsGroup } from './TasksTableColumnsSettingsGroup/TasksTableColumnsSettingsGroup';
import { EditFormControlButtonsPanel } from './EditFormControlButtonsPanel/EditFormControlButtonsPanel';

import {
  SELECT_OPTIONS_TYPE,
  FUNC_IS_REQUIRED_TYPE,
  TASKS_TYPES_SETTINGS_TYPE,
  OBJECT_OF_ANY_TYPE
} from '../../../constants/propTypes';

import cn from 'classnames';

import './style.css';

export class AdminSettingsEditForm extends Component {

  static propTypes = {
    isReadOnly: PropTypes.bool.isRequired,
    isCreating: PropTypes.bool.isRequired,
    selectedDepartment: PropTypes.string,
    departmentSelectOptions: SELECT_OPTIONS_TYPE.isRequired,
    selectDepartment: FUNC_IS_REQUIRED_TYPE,
    selectedEquipmentClass: PropTypes.string,
    equipmentClassSelectOptions: SELECT_OPTIONS_TYPE.isRequired,
    selectEquipmentClass: FUNC_IS_REQUIRED_TYPE,
    tasksTypesSettings: TASKS_TYPES_SETTINGS_TYPE.isRequired,
    editTasksTypesSettings: FUNC_IS_REQUIRED_TYPE,
    tasksTableColumnsSettings: OBJECT_OF_ANY_TYPE.isRequired,
    editTasksTableColumnsSettings: FUNC_IS_REQUIRED_TYPE,
    settingsSaveDisabledReasonText: PropTypes.string,
    onSettingsEditCancel: FUNC_IS_REQUIRED_TYPE,
    onSettingsSave: FUNC_IS_REQUIRED_TYPE
  };

  _renderNewSettingsOwnerSelects = () => {
    const {
      selectedDepartment,
      departmentSelectOptions,
      selectDepartment,
      selectedEquipmentClass,
      equipmentClassSelectOptions,
      selectEquipmentClass
    } = this.props;
    return(
      <NewSettingsOwnerSelects
          selectedDepartment={selectedDepartment}
          departmentSelectOptions={departmentSelectOptions}
          selectDepartment={selectDepartment}
          selectedEquipmentClass={selectedEquipmentClass}
          equipmentClassSelectOptions={equipmentClassSelectOptions}
          selectEquipmentClass={selectEquipmentClass}
      />
    );
  };

  _renderTasksTypesSettingsGroup = () => {
    const {
      isReadOnly,
      tasksTypesSettings,
      editTasksTypesSettings
    } = this.props;
    return(
      <TasksTypesSettingsGroup
          isReadOnly={isReadOnly}
          tasksTypesSettings={tasksTypesSettings}
          editTasksTypesSettings={editTasksTypesSettings}
      />
    );
  };

  _renderTasksTableColumnsSettingsGroup = () => {
    const {
      isReadOnly,
      tasksTableColumnsSettings,
      editTasksTableColumnsSettings
    } = this.props;
    return(
      <TasksTableColumnsSettingsGroup
          isReadOnly={isReadOnly}
          tasksTableColumnsSettings={tasksTableColumnsSettings}
          editTasksTableColumnsSettings={editTasksTableColumnsSettings}
      />
    );
  };

  _renderSettingsFormControlButtons = () => {
    const{
      settingsSaveDisabledReasonText,
      onSettingsEditCancel,
      onSettingsSave
    } = this.props;
    return(
      <EditFormControlButtonsPanel
          settingsSaveDisabledReasonText={settingsSaveDisabledReasonText}
          onSettingsEditCancel={onSettingsEditCancel}
          onSettingsSave={onSettingsSave}
      />
    );
  };

  render() {
    const {
      isReadOnly,
      isCreating
    } = this.props;
    return (
      <div
          className={
            cn(
              'admin-settings-edit-form',
              {'admin-settings-edit-form--read-only': isReadOnly }
            )
          }
      >
        {isCreating && this._renderNewSettingsOwnerSelects()}
        {this._renderTasksTypesSettingsGroup()}
        {this._renderTasksTableColumnsSettingsGroup()}
        {isReadOnly || this._renderSettingsFormControlButtons()}
      </div>
    );
  }
}