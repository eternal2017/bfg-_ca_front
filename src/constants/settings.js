/*
* Константы для работы с моделью БД settings. Особенность модели в том, что данные в ней хранятся в поле
* JSON и, фактически, здесь может храниться любая нужная фронту информация, без создания дополнительных моделей.
* Для сущности модели в БД уникальным ключом является комбинация "группы настройки" (то, к какой области настройка
* относится) и "имени настройки".
*
* Все настройки для приложения ca_frontend будут храниться в служебной группе @@CA_FRONTEND, чтобы они были уникальны,
* не пересекались с другими веб-приложениями, использующими модель settings. Т.е. признак уникальности настроек у
* сa_frontend, это имя (поле name)
* */
export const CA_FRONTEND_SETTINGS_GROUP = '@@ca_frontend';

export const MAIN_SETTINGS_ID = 'MAIN_SETTINGS_ID';
export const MAIN_SETTINGS_TITLE = 'Основные настройки';

export const CREATE_NEW_SETTINGS = 'Создать новые настройки';
export const CREATE_SETTINGS_USING_SELECTED_AS_TEMPLATE = 'Создать новые настройки на основании выбранных';

export const TASKS_TYPES_SETTINGS_GROUP_TITLE = '1. Отображаемые типы заданий';

export const TASKS_TABLE_COLUMNS_SETTINGS_GROUP_TITLE = '2. Порядок и видимость колонок отображаемой таблицы заданий';

export const CHOOSE_DEPARTMENT_FOR_SETTINGS = 'Выберите подразделение для настроек';
export const CHOOSE_EQUIPMENT_CLASS_FOR_SETTINGS = 'Выберите класс РЦ для настроек (не обязательно)';

export const DEPARTMENT_SHOULD_BE_CHOOSEN_FOR_SETTINGS = 'Для новых настроек нужно выбрать хотя бы подразделение';
export const SETTINGS_FOR_DEPARTMENT_ALREADY_EXISTS =
  'Настройки для выбранного подразделения уже существуют';
export const SETTINGS_FOR_EQUIPMENT_CLASS_IN_DEPARTMENT_ALREADY_EXISTS =
  'Настройки для выбранного класса РЦ в подразделения уже существуют';

export const ADMIN_SETTINGS_REACT_SELECTS_OVERRIDE_STYLES = {
  control: (base, {isDisabled}) => isDisabled ?
    base :
    ({
      ...base,
      'background-color': '#fff'
    }),
  placeholder: base => ({
    ...base,
    'font-size': '1rem'
  })
};

export const ADMIN_SETTINGS_VIEW_MODE = {
  READ_ONLY: 'READ_ONLY',
  CREATE: 'CREATE',
  EDIT: 'EDIT'
};