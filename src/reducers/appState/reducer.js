import { SET_APP_STATUS, SET_APP_AUTH_SESSION_ID } from './actions';

export const APP_STATUS = {
  NOT_INITIALIZED: 'NOT_INITIALIZED',
  NO_TASKS_IN_SYSTEM: 'NO_TASKS_IN_SYSTEM',
  READY_TO_WORK: 'READY_TO_WORK'
};

const APP_INITIAL_STATE = {
  status: APP_STATUS.NOT_INITIALIZED,
  authSessionId: null
};

export const appState = (state = {...APP_INITIAL_STATE}, action) => {
  switch(action.type){
  case SET_APP_STATUS:
    return {...state, status: action.appStatus};
  case SET_APP_AUTH_SESSION_ID:
    return {...state, authSessionId: action.authSessionId};
  default:
    return state;
  }
};