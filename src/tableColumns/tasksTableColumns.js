export const TASKS_TABLE_COLUMNS = {
  orderName: {
    id: 'orderName',
    title: 'Наименование заказа',
    order: 0,
    visible: true,
    minWidth: 140
  },
  entityIdentity: {
    id: 'entityIdentity',
    title: 'Клиентский идентификатор Номенклатуры',
    order: 1,
    visible: false,
    minWidth: 200
  },
  entityCode: {
    id: 'entityCode',
    title: 'Шифр Номенклатуры',
    order: 2,
    visible: true,
    minWidth: 200
  },
  entityName: {
    id: 'entityName',
    title: 'Наименование Номенклатуры',
    order: 3,
    visible: true,
    minWidth: 250
  },
  entityBatchIdentity: {
    id: 'entityBatchIdentity',
    title: 'Клиентский идентификатор партии',
    order: 4,
    visible: true,
    minWidth: 140
  },
  entityBatchAmountForTask: {
    id: 'entityBatchAmountForTask',
    title: 'Размер партии на задание',
    order: 5,
    visible: true,
    minWidth: 100
  },
  entityRouteIdentity: {
    id: 'entityRouteIdentity',
    title: 'Клиентский идентификатор маршрута',
    order: 6,
    visible: false,
    minWidth: 140
  },
  isMainEntityRouteTitle: {
    id: 'isMainEntityRouteTitle',
    title: 'Основной / Альтернативный маршрут',
    order: 7,
    visible: false,
    minWidth: 140
  },
  operationTypeTitle: {
    id: 'operationTypeTitle',
    title: 'Тип операции',
    order: 7,
    visible: true,
    minWidth: 100
  },
  operationIdentity: {
    id: 'operationIdentity',
    title: 'Клиентский идентификатор операции',
    order: 8,
    visible: false,
    minWidth: 140
  },
  operationNumber: {
    id: 'operationNumber',
    title: 'Номер операции',
    order: 9,
    visible: true,
    minWidth: 100
  },
  operationName: {
    id: 'operationName',
    title: 'Наименование операции',
    order: 10,
    visible: true,
    minWidth: 200
  },
  operationDurationByTechnologyInHours: {
    id: 'operationDurationByTechnologyInHours',
    title: 'Длительность операции по технологии, ч.',
    order: 11,
    visible: true,
    minWidth: 120
  },
  operationStartDate: {
    id: 'operationStartDate',
    title: 'Плановые дата и время начала выполнения задания',
    order: 12,
    visible: true,
    minWidth: 150
  },
  operationStopDate: {
    id: 'operationStopDate',
    title: 'Плановые дата и время окончания выполнения задания',
    order: 13,
    visible: true,
    minWidth: 150
  }
};