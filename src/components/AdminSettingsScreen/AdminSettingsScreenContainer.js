import { compose } from 'redux';
import { connect } from 'react-redux';

import { asyncComponent } from '../../hoc/asyncComponent/asyncComponent';

import { setDepartmentForEquipmentClassSelectOptions, showAdminSettings } from '../../reducers/adminSettings/actions';
import { fetchDepartments } from '../../operations/departments';
import { fetchSettings, saveSettingsEntity, deleteSettings } from '../../operations/settings';
import { fetchEquipmentClassesInDepartment } from '../../operations/equipmentClasses';

import { appStatusSelector } from '../../reducers/appState/selectors';
import {
  adminSettingsToShowSelector,
  departmentForEquipmentClassSelectOptionsSelector
} from '../../reducers/adminSettings/selectors';

import {
  adminSelectedSettingsDataSelector,
  adminSettingsDepartmentSelectOptionsSelectors,
  adminSettingsEntitiesSelector,
  adminSettingsEquipmentClassSelectOptionsSelectors,
  adminSettingsSelectOptionsSelector
} from '../../selectors/adminSettings';

import { APP_STATUS } from '../../reducers/appState/reducer';

import { MAIN_SETTINGS_ID } from '../../constants/settings';

import { AdminSettingsScreen } from './AdminSettingsScreen';

const mapStateToProps = state => {
  const selectedSettingsId = adminSettingsToShowSelector(state);
  const selectedSettingsData = adminSelectedSettingsDataSelector(state, { settingsId: selectedSettingsId });
  const isMainSettingsSelected = selectedSettingsId === MAIN_SETTINGS_ID;
  const settingsEntities = adminSettingsEntitiesSelector(state);
  return{
    appStatus: appStatusSelector(state),
    settingsSelectOptions: adminSettingsSelectOptionsSelector(state),
    selectedSettingsId,
    selectedSettingsData,
    isMainSettingsSelected,
    departmentOptions: adminSettingsDepartmentSelectOptionsSelectors(state),
    equipmentClassOptions: adminSettingsEquipmentClassSelectOptionsSelectors(
      state,
      { departmentId: departmentForEquipmentClassSelectOptionsSelector(state)}
    ),
    settingsEntities
  }
};

const mapDispatchToProps = {
  showAdminSettings,
  deleteSettings,
  setDepartmentForEquipmentClassSelectOptions,
  fetchEquipmentClassesInDepartment,
  fetchSettings,
  fetchDepartments,
  saveSettingsEntity
};

const mergeProps = (stateProps, dispatchProps) => {
  const {
    appStatus,
    selectedSettingsId
  } = stateProps;
  const {
    showAdminSettings,
    deleteSettings,
    setDepartmentForEquipmentClassSelectOptions,
    fetchEquipmentClassesInDepartment,
    fetchSettings,
    fetchDepartments,
    saveSettingsEntity
  } = dispatchProps;
  return {
    ...stateProps,
    selectSettings: showAdminSettings,
    setDepartmentForEquipmentClassSelectOptions,
    fetchEquipmentClassesInDepartment,
    deleteSelectedSettings: () => deleteSettings(selectedSettingsId),
    saveSettings: saveSettingsEntity,
    fetchRequiredData: () => {
      if(appStatus === APP_STATUS.NOT_INITIALIZED) return Promise.resolve();
      return Promise.all([
        fetchSettings(),
        fetchDepartments()
      ])
    }
  };
};

export const AdminSettingsScreenContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  asyncComponent({
    resolve: [
      {
        fn: ({fetchRequiredData}) => fetchRequiredData()
      }
    ]
  })
)(AdminSettingsScreen);

AdminSettingsScreenContainer.displayName = 'AdminSettingsScreenContainer';