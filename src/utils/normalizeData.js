import { normalize, schema } from 'normalizr';

export const normalizeData = (data, keys = []) => {

  const schemaToNormalize = keys
    .reduce((schemaToNormalize, key) => {
      schemaToNormalize[key] = [new schema.Entity(key)];
      return schemaToNormalize;
    }, {});

  return normalize(data, schemaToNormalize)
};