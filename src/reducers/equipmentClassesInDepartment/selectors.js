export const equipmentClassesInDepartmentStateSelector = state => state.equipmentClassesInDepartment;

export const equipmentClassesInDepartmentEntitiesSelector = (state, { departmentId }) =>
  equipmentClassesInDepartmentStateSelector(state)[departmentId];