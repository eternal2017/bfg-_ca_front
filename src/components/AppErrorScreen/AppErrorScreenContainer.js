import { connect } from 'react-redux';

import { appErrorMsgSelector } from '../../reducers/appState/selectors';

import { AppErrorScreen } from './AppErrorScreen';

const mapStateToProps = state => ({
  appErrorMsg: appErrorMsgSelector(state)
});

export const AppErrorScreenContainer = connect(
  mapStateToProps
)(AppErrorScreen);

AppErrorScreenContainer.displayName = 'AppErrorScreenContainer';