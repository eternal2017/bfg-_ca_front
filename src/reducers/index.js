import { combineReducers } from 'redux';

import { blockingAsyncAction } from './blockingAsyncAction/reducer';
import { appState } from './appState/reducer';
import { notification } from './notification/reducer';
import { entities } from './entities/reducer';
import { tasksMainFilters } from './tasksMainFilters/reducer';
import { equipmentClassesInDepartment } from './equipmentClassesInDepartment/reducer';
import { tasks } from './tasks/reducer';
import { adminSettings } from './adminSettings/reducer';

const rootReducer = combineReducers({
  notification,
  blockingAsyncAction,
  appState,
  entities,
  tasksMainFilters,
  equipmentClassesInDepartment,
  tasks,
  adminSettings
});

export default rootReducer;