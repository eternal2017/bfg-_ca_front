import { createSelector } from 'reselect';

import _get from 'lodash/get';

export const entitiesStateSelector = state => state.entities;

export const entitySelector = (state, { model, id }) =>
  _get(entitiesStateSelector(state), [model, id]);

export const entitiesForModelSelector = (state, { model }) =>
  entitiesStateSelector(state)[model];

export const entitiesForModelSelectorFactory = model => createSelector(
  state => entitiesForModelSelector(state, { model }),
  entities => entities || {}
);

export const entitiesListForModelSelectorFactory = model => createSelector(
  state => entitiesForModelSelector(state, { model }),
  entities => Object.values(entities || {})
);