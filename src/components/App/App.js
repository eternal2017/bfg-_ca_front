import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { AdminSettingsScreenContainer } from '../AdminSettingsScreen/AdminSettingsScreenContainer';

import { AssistantNavigationPanelContainer } from '../AssistantNavigationPanel/AssistantNavigationPanelContainer';
import { DepartmentChooseScreenContainer } from '../TasksOwnerChooseScreen/DepartmentChooseScreenContainer';
import { EquipmentClassChooseScreenContainer } from '../TasksOwnerChooseScreen/EquipmentClassChooseScreenContainer';
import { TasksViewScreenContainer } from '../TasksViewScreen/TasksViewScreenContainer';

import { AppErrorScreenContainer } from '../AppErrorScreen/AppErrorScreenContainer';

import { Switch, Route } from 'react-router-dom';

import { APP_STATUS } from '../../reducers/appState/reducer';
import { NOTIFICATION_LEVEL } from '../../constants/notification';
import { FUNC_IS_REQUIRED_TYPE, NUMBER_OR_STRING_TYPE } from '../../constants/propTypes';

import { APP_INITIALIZATION_SUCCESSFULLY_COMPLETE_MSG } from '../../messages/appState';

import './style.css';

export class App extends Component {

  static propTypes = {
    appStatus: PropTypes.oneOf(Object.values(APP_STATUS)),
    appAuthSessionId: PropTypes.string,
    logIn: FUNC_IS_REQUIRED_TYPE,
    sendNotification: FUNC_IS_REQUIRED_TYPE,
    simulationSessionIdTasksFilter: NUMBER_OR_STRING_TYPE
  };

  componentDidMount(){
    const {
      sendNotification,
      appStatus
    } = this.props;
    if(appStatus === APP_STATUS.READY_TO_WORK)
      sendNotification(APP_INITIALIZATION_SUCCESSFULLY_COMPLETE_MSG, NOTIFICATION_LEVEL.SUCCESS);
  }

  _renderAssistantWorkSpace = () =>
    <React.Fragment>
      {this._renderWorkSpaceNavigationPanel()}
      {this._renderWorkSpaceContent()}
    </React.Fragment>;

  _renderWorkSpaceNavigationPanel = () =>
    <div className="cyber-assistant-app__work-space-navigation-panel">
      <AssistantNavigationPanelContainer />
    </div>;

  _renderWorkSpaceContent = () =>
    <div className="cyber-assistant-app__work-space-content">
      {
        this.props.appStatus === APP_STATUS.READY_TO_WORK ?
          this._renderWorkSpaceContentRoutes() :
          this._renderAppErrorScreen()
      }
    </div>;

  _renderWorkSpaceContentRoutes = () => {
    /*
    * Приложение авторизуется как сервис, т.е. в конфигурации вебсервера указываются определенные логин и пароль, по
    * которому сервис каждый раз успешно авторизуется, если конфигурация правильная (случаи неправильной конфигурации
    * отрабатываются в экшене logIn()). НО, авторизационные куки имеют свойство истекать и наш "сервис" разлогинивается,
    * это может случиться в процессе работы в приложении (если 2 дня не перезагружали страницу, т.к. при каждой
    * перезагрузке мы логинимся заново и куки обновляются, т.е., это достаточно редкий случай, но, всё же, возможный).
    * Т.к. узнаем мы о том, что куки истекли, только когда в ответе на очередной запрос приходит 401, то логичным
    * для пользователей было бы следующее поведение - Получаем ошибку 401, автоматически авторизуемся повторно,
    * ещё раз выполняем запрос для которого возникла 401 ошибка, чтобы безболезненно для пользователя продолжить работу
    * в системе. В данный момент это реализовано следующим образом:
    *  - Все запросы (кроме админского роута, там другая логика) производятся только на didMount компонентов.
    *  - Все компоненты обернуты в asyncComponent и в случае 401 ошибки на запрос, они не рендерятся
    *  - Если приходит ошибка 401 на любой запрос, в абстракции запроса, сразу же вызывается экшен logIn
    *  - При каждой успешной авторизации в state запоминается новый идентификатор авторизационной сессии appAuthSessionId,
    *  просто при помощи uuid (новый идентфикатор будет только если пришла 401 ошибка и мы повторно успешно авторизовались,
    *  во всех других случаях, идентификатор будет неизменным, полученным при инициализации приложения)
    *  - Этот идентификатор используется как первая часть атрибута key у реакт компонента обертки основных роутов, т.е. мы говорим
    *  реакту при повторной авторизации, что компонент нужно отрендерить новый. Поэтом происходит анмаунт старого и
    *  маунт нового такого же компонента, снова выполняются все нужные запросы на didDmount и состояние asyncComponent'а
    *  сбрасывается, теперь оно не ошибочное (как было после 401 ошибки ранее) и получаем повторное выполнение нужного
    *  запроса после 401 ошибки и авторизации.
    *
    * Возможно, это в будущем изменится, но сейчас нужно учитывать эту логику в виду при расширении функционала. Если
    * появятся запросы, которые производятся не на маунт компонента, то нужно будет продумать другую реализацию, сейчас
    * это подходит, т.к. это приложение только отображает данные и не создаёт или удаляет какие-то сущности. Для
    * админской панели, где есть CRUD, логика немного другая.
    *
    * Второй составной частью атрибута key у реакт компонента обертки основных роутов является идентификатор основной
    * сессии моделирования, по которой отображаются все данные в приложении: подразделения и классы РЦ отображаются
    * только те, для которых есть задания по текущей сесии, ну и сами задания всегда отображаются только для текущей
    * основной сессии. Поэтому когда сессия изменяется (получаем об этом сообщение по веб-сокету и записываем новый
    * идентификатор в стор), нужно перезапросить данные приложения, на каком бы экране пользователь не находился.
    * Можно было бы везде добавить componentDidUpdate и писать везде проверки на смену сессии, но, как уже было указано,
    * т.к. все запросы выполняются на didMount всех asyncComponent'ов, то легче просто добавить это значение в ключ
    * реакт компонента обертки всех роутов, чтобы компонент просто перемаунтился по-новому ключу и перезапросил данные.
    *
    * Теоретически, в данный момент хватило бы и уникального id сессии авторизации, т.к., при смене основной сессии?
    * приложение сейчас всегда переавторизуется (описано подробнее зачем в src/api/socketApi/actions.js в
    * функции handleSimulationSessionForTasksChangedEvent), т.е. обе части ключа изменяются одновременно, но это может
    * измениться в будущем (может добавим проверку на авторизацию или, вообще, не нужно будет переавторизовываться),
    * поэтому ключ составной
    * */
    const {
      appAuthSessionId,
      simulationSessionIdTasksFilter
    } = this.props;
    const mainRoutesComponentWrapKey = [appAuthSessionId, simulationSessionIdTasksFilter].join('_');
    return(
      <Switch key={mainRoutesComponentWrapKey}>
        <Route path="/" exact component={DepartmentChooseScreenContainer}/>
        <Route path="/:departmentIdentity/:equipmentClassIdentity" component={TasksViewScreenContainer}/>
        <Route path="/:departmentIdentity" component={EquipmentClassChooseScreenContainer}/>
      </Switch>
    );
  };

  _renderAppErrorScreen = () => <AppErrorScreenContainer />;

  render(){
    return(
      <div className="cyber-assistant-app">
        <Switch>
          <Route path="/admin" exact component={AdminSettingsScreenContainer}/>
          <Route path="/" render={this._renderAssistantWorkSpace}/>
        </Switch>
      </div>
    );
  }
}