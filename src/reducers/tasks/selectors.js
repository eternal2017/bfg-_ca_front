export const tasksDataStateSelector = state => state.tasks;

export const tasksDataSelector = (state, { tasksDataUniqId }) =>
  tasksDataStateSelector(state)[tasksDataUniqId];