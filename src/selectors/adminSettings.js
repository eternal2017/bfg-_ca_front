import { createSelector } from 'reselect';

import {
  entitiesForModelSelectorFactory,
  entitiesListForModelSelectorFactory
} from '../reducers/entities/selectors';
import { departmentEntitiesSelector, departmentsListSortedByIdentitySelector } from './department';
import {
  equipmentClassesEntitiesSelector,
  equipmentClassesInDepartmentListSortedByIdentitySelector
} from './equipmentClassesInDepartment';

import { TASKS_TABLE_COLUMNS } from '../tableColumns/tasksTableColumns';

import { SETTINGS_MODEL} from '../constants/models';
import { MAIN_SETTINGS_ID, MAIN_SETTINGS_TITLE } from '../constants/settings';
import { DEFAULT_TASKS_TYPES_SETTINGS } from '../constants/tasks';

import { MAIN_SETTINGS_DESCRIPTION } from '../messages/adminSettings';

import { fieldComparatorFactory, stringComparator } from '../utils/arrayComparators';

import _get from 'lodash/get';
import _mapValues from 'lodash/mapValues';
import { DEPARTMENT_LABEL, EQUIPMENT_CLASS_LABEL } from '../constants/labels';

export const adminSettingsEntitiesSelector = entitiesForModelSelectorFactory(SETTINGS_MODEL);

export const adminSettingsEntitiesListSelector = entitiesListForModelSelectorFactory(SETTINGS_MODEL);

export const adminSettingsSelectOptionsSelector = createSelector(
  departmentEntitiesSelector,
  equipmentClassesEntitiesSelector,
  adminSettingsEntitiesListSelector,
  (departmentEntities, equipmentClassesEntities, adminSettingsEntitiesList) => {
    const mainSettingsOption = {
      optionValue: MAIN_SETTINGS_ID,
      optionLabel: `${MAIN_SETTINGS_TITLE} (${MAIN_SETTINGS_DESCRIPTION})`,
      optionPayload: {
        settingsId: MAIN_SETTINGS_ID
      }
    };
    const settingsOptions = adminSettingsEntitiesList
      .filter(({id}) => id !== MAIN_SETTINGS_ID)
      .sort(fieldComparatorFactory(stringComparator, 'id'))
      .map(({id, data}) => {
        /*
        * Мы вынуждены хранить в настройках в БД и клиентский идентификатор и имя, чтобы иметь возможность отобразить их
        * в случае, если после переимпорта данных таких сущностей уже нет в системе
        * */
        const {
          departmentIdentity,
          departmentName,
          equipmentClassIdentity,
          equipmentClassName
        } = data;

        const departmentTitle = getSelectOptionTitle(DEPARTMENT_LABEL, departmentIdentity, departmentName);
        const equipmentClassTitle =
          getSelectOptionTitle(EQUIPMENT_CLASS_LABEL, equipmentClassIdentity, equipmentClassName);

        return {
          optionValue: id,
          optionLabel: [departmentTitle, equipmentClassTitle]
            .filter(title => title !== null)
            .join(', '),
          optionPayload: {
            settingsId: id,
            departmentIdentity,
            departmentName,
            equipmentClassIdentity,
            equipmentClassName
          }
        };
      });

    return [mainSettingsOption].concat(settingsOptions);
  }
);

const getSelectOptionTitle = (label, identity, name) => {
  if(!identity) return null;
  return [
    label,
    getEntityFullName(identity, name)
  ].join(': ');
};

const getEntityFullName = (identity, name) => [
  name,
  `(${identity})`
].join(' ');

export const adminSelectedSettingsDataSelector = createSelector(
  adminSettingsEntitiesSelector,
  (_, { settingsId }) => settingsId,
  (settingsEntities, settingsId) => {
    const { data = {} } = _get(settingsEntities, [settingsId], {});
    /*
    * Мы вынуждены хранить в настройках в БД и клиентский идентификатор и имя, чтобы иметь возможность отобразить их
    * в случае, если после переимпорта данных таких сущностей уже нет в системе
    * */
    const {
      departmentIdentity,
      departmentName,
      equipmentClassIdentity,
      equipmentClassName,
      tasksTypes = {...DEFAULT_TASKS_TYPES_SETTINGS},
      tasksTableColumns = {}
    } = data;

    return {
      departmentIdentity,
      departmentName,
      equipmentClassIdentity,
      equipmentClassName,
      tasksTypes,
      tasksTableColumns: _mapValues(
        TASKS_TABLE_COLUMNS,
        (columnData, columnKey) => ({
          ...columnData,
          ...tasksTableColumns[columnKey]
        })
      )
    };
  }
);

export const adminSettingsDepartmentSelectOptionsSelectors = createSelector(
  departmentsListSortedByIdentitySelector,
  departmentsList => departmentsList
    .map(({id: departmentId, identity: departmentIdentity, name: departmentName}) => ({
      optionValue: departmentIdentity,
      optionLabel: getEntityFullName(departmentIdentity, departmentName),
      optionPayload: {
        departmentId,
        departmentIdentity,
        departmentName
      },
    }))
);

export const adminSettingsEquipmentClassSelectOptionsSelectors = createSelector(
  equipmentClassesInDepartmentListSortedByIdentitySelector,
  equipmentClassList => equipmentClassList
    .map(({identity: equipmentClassIdentity, name: equipmentClassName}) => ({
      optionValue: equipmentClassIdentity,
      optionLabel: getEntityFullName(equipmentClassIdentity, equipmentClassName),
      optionPayload: {
        equipmentClassIdentity,
        equipmentClassName
      }
    }))
);