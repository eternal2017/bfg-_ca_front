import { compose } from 'redux';
import { connect } from 'react-redux';

import { asyncComponent } from '../../hoc/asyncComponent/asyncComponent';

import { sendNotification } from '../../reducers/notification/actions';

import { initApp, logIn } from '../../operations/app';

import { appStatusSelector, appAuthSessionIdSelector } from '../../reducers/appState/selectors';
import { simulationSessionTasksFilterSelector } from '../../reducers/tasksMainFilters/selectors';

import { App } from './App';

const mapStateToProps = state => {
  const { id: simulationSessionIdTasksFilter } = simulationSessionTasksFilterSelector(state);
  return {
    /*
    * Верхнеуровневый реакт-роутер передаёт параметры роутинга потомкам через контекст, а не через пропсы, в итоге, т.к.
    * это контейнер, то если не передать в пропсы state.router, который изменяется при навигации, то контейнер никак
    * не узнает, что что-то изменилось и не перерисует компонент App, где определены динамические роуты. Т.е. если мы
    * хотим использовать здесь контейнер, то по-другому никак. Теоретически, если это начнет смущать, то можно как-то
    * переформатировать код, перенести ConnectedRouter в компонент App или куда-то роуты перенести выше главного
    * контейнереа, но мне видится это, как ненужная адаптация реакт-редакс приложения под реакт роутер, которая не
    * кажется логичной, а только вносит ограничения по структуре дерева компонентов. Так что, лучше просто передать проп, ИМХО
    * */
    router: state.router,
    appStatus: appStatusSelector(state),
    appAuthSessionId: appAuthSessionIdSelector(state),
    simulationSessionIdTasksFilter
  }
};

const mapDispatchToProps = {
  initApp,
  logIn,
  sendNotification
};

export const AppContainer = compose(
  connect(mapStateToProps, mapDispatchToProps),
  asyncComponent({
    resolve: [
      {
        fn: ({initApp}) => initApp()
      }
    ],
    shouldRenderOnError: true
  })
)(App);