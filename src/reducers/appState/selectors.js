import { createSelector } from 'reselect';

import { APP_STATUS } from './reducer';

import {
  ERROR_BY_APP_INITIALIZATION_TITLE,
  ERROR_BY_APP_INITIALIZATION_MSG,
  NO_TASKS_IN_SYSTEM_TITLE,
  NO_TASKS_IN_SYSTEM_MSG
} from '../../messages/appState';

export const appStateSelector = state => state.appState;

export const appStatusSelector = state => appStateSelector(state).status;

export const appAuthSessionIdSelector = state => appStateSelector(state).authSessionId;

export const appErrorTitleSelector = createSelector(
  appStatusSelector,
  appStatus => {
    if(appStatus === APP_STATUS.NOT_INITIALIZED) return ERROR_BY_APP_INITIALIZATION_TITLE;
    if(appStatus === APP_STATUS.NO_TASKS_IN_SYSTEM) return NO_TASKS_IN_SYSTEM_TITLE;
  }
);

export const appErrorMsgSelector = createSelector(
  appStatusSelector,
  appStatus => {
    if(appStatus === APP_STATUS.NOT_INITIALIZED) return ERROR_BY_APP_INITIALIZATION_MSG;
    if(appStatus === APP_STATUS.NO_TASKS_IN_SYSTEM) return NO_TASKS_IN_SYSTEM_MSG;
  }
);
