import { createSelector } from 'reselect';

import { getTasksDataUniqId } from '../reducers/tasks/actions';

import { tasksDataStateSelector } from '../reducers/tasks/selectors';
import {
  departmentTasksFilterSelector,
  equipmentClassTasksFilterSelector,
  simulationSessionTasksFilterSelector
} from '../reducers/tasksMainFilters/selectors';


import { adminSettingsEntitiesSelector } from './adminSettings';

import { TASKS_TABLE_COLUMNS } from '../tableColumns/tasksTableColumns';

import { MAIN_SETTINGS_ID } from '../constants/settings';
import {
  ENTITY_MODEL,
  ENTITY_ROUTE_MODEL,
  OPERATION_MODEL,
  ORDER_MODEL,
  SIMULATION_ENTITY_BATCH_MODEL,
  SIMULATION_OPERATION_TASK_EQUIPMENT_MODEL,
  SIMULATION_OPERATION_TASK_MODEL,
  SIMULATION_ORDER_ENTITY_BATCH_MODEL,
} from '../constants/models';
import {
  DEFAULT_TASKS_TYPES_SETTINGS,
  TASK_TYPES,
  TASK_TYPES_DURATION_IN_TECHNOLOGY_FIELD_MAP,
  TASK_TYPES_TITLES_MAP
} from '../constants/tasks';
import {
  ALTERNATE_LABEL, MAIN_LABEL,
  NOT_PLANNED_LABEL,
  WITHOUT_ORDER_LABEL,
  WITHOUT_ORDER_PRIORITY_LABEL
} from '../constants/labels';
import { FORMAT_FULL_TIME } from '../constants/dateFormats';
import { SECS_IN_HOUR } from '../constants/magics';

import { getEntityDataByTemplate } from '../utils/entities';

import {
  complexComparatorFactory,
  fieldComparatorFactory,
  numberComparator,
  stringComparator
} from '../utils/arrayComparators';

import moment from 'moment';
import formatNumber from 'format-number';

import _partialRight from 'lodash/partialRight';
import _mapValues from 'lodash/mapValues';
import _get from 'lodash/get';

export const currentTasksTableSettingsEntitySelector = createSelector(
  departmentTasksFilterSelector,
  equipmentClassTasksFilterSelector,
  adminSettingsEntitiesSelector,
  (departmentTasksFilter, equipmentClassTasksFilter, adminSettingsEntities) => {
    const { identity: departmentIdentityTasksFilter } = departmentTasksFilter;
    const { identity: equipmentClassIdentityTasksFilter } = equipmentClassTasksFilter;
    const equipmentClassInDepartmentSettingsId = [
      departmentIdentityTasksFilter,
      equipmentClassIdentityTasksFilter
    ].join('_');
    /*
    * Ищем настройки сначала по классу РЦ в подразделения, если таких нет, то проверяем есть ли общие настройки
    * по подразделению, если и таких нет то выбираем общие настройки.
    * */
    return adminSettingsEntities[equipmentClassInDepartmentSettingsId] ||
      adminSettingsEntities[departmentIdentityTasksFilter] ||
      adminSettingsEntities[MAIN_SETTINGS_ID];
  }
);

export const currentTasksTableColumnsSettingsSelector = createSelector(
  currentTasksTableSettingsEntitySelector,
  settingsEntity => _get(settingsEntity, ['data', 'tasksTableColumns'])
);

export const currentTasksDataSelector = createSelector(
  simulationSessionTasksFilterSelector,
  departmentTasksFilterSelector,
  equipmentClassTasksFilterSelector,
  tasksDataStateSelector,
  (simulationSessionTasksFilter, departmentTasksFilter, equipmentClassTasksFilter, tasksDataState) => {
    const { id: simulationSessionIdTasksFilter } = simulationSessionTasksFilter;
    const { id: departmentIdTasksFilter } = departmentTasksFilter;
    const { id: equipmentClassIdTasksFilter } = equipmentClassTasksFilter;
    return tasksDataState[getTasksDataUniqId(
      simulationSessionIdTasksFilter,
      departmentIdTasksFilter,
      equipmentClassIdTasksFilter
    )]
  }
);

export const currentTasksTypesSettingsSelector = createSelector(
  currentTasksTableSettingsEntitySelector,
  settingsEntity => _get(settingsEntity, ['data', 'tasksTypes'])
);

export const currentTasksTableColumnsSelector = createSelector(
  currentTasksTableColumnsSettingsSelector,
  currentTasksTableColumnsSettings => {

    /*
    * Если в системе совсем нет никаких настроек, даже основных, то берется модель колонок по-умолчанию
    * TASKS_TABLE_COLUMNS
    * */
    const tableColumnsData = currentTasksTableColumnsSettings ?
      _mapValues(
        TASKS_TABLE_COLUMNS,
        (defaultColumnData, columnId) => ({
          ...defaultColumnData,
          ...currentTasksTableColumnsSettings[columnId]
        })
      ) :
      TASKS_TABLE_COLUMNS;

    return Object
      .values(tableColumnsData)
      .sort(fieldComparatorFactory(numberComparator, 'order'))
      .map(({
        id,
        title,
        visible,
        width,
        minWidth
      }) => ({
        Header: title,
        accessor: id,
        show: visible,
        width,
        minWidth
      }))
  }
);


export const currentTasksTableDataSelector = createSelector(
  currentTasksDataSelector,
  currentTasksTypesSettingsSelector,
  /*
  * Если в системе совсем нет никаких настроек, даже основных, то беруться настройки по-уолчанию
  * */
  (tasksData, currentTasksTypesSettings = DEFAULT_TASKS_TYPES_SETTINGS) => {
    if(!tasksData) return [];

    const {
      [SIMULATION_OPERATION_TASK_EQUIPMENT_MODEL]: simulationOperationTaskEquipmentEntities = {},
      [SIMULATION_OPERATION_TASK_MODEL]: simulationOperationTaskEntities = {},
      [OPERATION_MODEL]: operationEntities = {},
      [ENTITY_ROUTE_MODEL]: entityRouteEntities = {},
      [SIMULATION_ENTITY_BATCH_MODEL]: simulationEntityBatchEntities = {},
      [ENTITY_MODEL]: entityModelEntities = {},
      [SIMULATION_ORDER_ENTITY_BATCH_MODEL]: simulationOrderEntityBatchEntities = {},
      [ORDER_MODEL]: orderEntities = {}
    } = tasksData;

    /*
    * В таблице нужно вывести задания с указанием начала и конца. Если задание разделяется концом смены или обедом, то в
    * БД для такого задания хранятся только его подзадания. Подзадания отображать не нужно, поэтому  для таких случаев
    * отображается основная информация по первому из подзаданий, но время конца записывается как время конца последнего
    * из подзаданий (т.е. как будто бы отображаем задание просто с учетом перерывов в дате окончания),
    * другая информация по остальным подзаданиям игнорируется.
    * Какого-то критерия, что подзадания являются частями одного задания нет, поэтому сами определяем уникальный
    * ключ, определяющий связанность подзаданий в задание, сейчас это operationId_entityBatchId_type, т.е. несколько
    * заданий определенного типа на определенную операцию для одной партии не имеет смысла и никогда не выполнится, если
    * такое наблюдается, то это всё подзадания одного поделенного задания.
    * При первом проходе записываем по указанным идентификаторам время окончания последних подзаданий, а при втором проходе,
    * когда будет осуществляться фильтрация подзаданий, для первых подзаданий перезапишем эту стартовую дату
    * */
    let lastSubTasksStopDatesMap = {};

    const allTasksTableData = Object
      .values(simulationOperationTaskEquipmentEntities)
      .map(({simulationOperationTaskId}) => {

        const simulationOperationTaskEntity = simulationOperationTaskEntities[simulationOperationTaskId];
        const simulationOperationTaskParams = _getSimulationOperationTaskParams(simulationOperationTaskEntity);

        const { operationId, operationType, entityBatchAmountForTask } = simulationOperationTaskParams;
        const operationParams = _getOperationParams(operationEntities[operationId], operationType, entityBatchAmountForTask);

        const { entityRouteId } = operationParams;
        const entityRouteParams = _getEntityRouteParams(entityRouteEntities[entityRouteId]);

        const { entityBatchId } = simulationOperationTaskParams;
        const simulationEntityBatchParams = _getSimulationEntityBatchParams(simulationEntityBatchEntities[entityBatchId]);

        const { entityId } = simulationEntityBatchParams;
        const entityParams = _getEntityParams(entityModelEntities[entityId]);

        const simulationOrderEntityBatchEntity = Object
          .values(simulationOrderEntityBatchEntities)
          // eslint-disable-next-line
          .find(simulationOrderEntityBatchEntity => simulationOrderEntityBatchEntity.simulationEntityBatchId == entityBatchId);
        const orderParams = _getOrderParams(simulationOrderEntityBatchEntity, orderEntities);

        /*
        * Если это последнее подзадание, то записываем дату его окончания по уникальному ключу подазадний, относящихся к
        * одному заданию, т.к. именно эту информацию нужно будет вывести по всему заданию.
        *
        * Дополнительно проверяем subTaskIndex, т.к. если это не подзадание, а задание, то он равен null, для такого случая
        * записывать информацию не нужно, т.к. задание не делилось и дату начала и конца перезаписывать уже не нужно
        * */
        const {
          subTaskIndex,
          totalSubTaskAmount,
          operationStopDate
        } = simulationOperationTaskParams;
        if(subTaskIndex !== null && subTaskIndex === totalSubTaskAmount){
          lastSubTasksStopDatesMap[_getSubTasksOfOneTaskKey(simulationOperationTaskParams)] = operationStopDate;
        }

        return {
          ...simulationOperationTaskParams,
          ...operationParams,
          ...entityRouteParams,
          ...simulationEntityBatchParams,
          ...entityParams,
          ...orderParams
        }
      });

    /*
    * Из всех данных оставляем только данные заданий и первых подзаданий (если задание разделено) и для первых заданий
    * перезаписываем дату окончания из словаря, которые мы собрали на предыдущем этапе, чтобы отобразить дату окончания
    * последнего подзадания.
    * Может случиться, что при фильтрации данных, последний сабтаск не попал в выбору и ключа для задания нет в
    * собранном словаре, в этом случае будем выводить служебные заголовок в ячейке
    * */
    return allTasksTableData
      .filter(({subTaskIndex, operationType}) =>
        (subTaskIndex === null || subTaskIndex === 1) && currentTasksTypesSettings[operationType]
      )
      .sort(complexComparatorFactory(
        fieldComparatorFactory(numberComparator, 'operationStartTime'),
        fieldComparatorFactory(numberComparator, 'entityId'),
        fieldComparatorFactory(stringComparator, 'operationNumber')
      ))
      .map(taskTableData => {
        const {
          operationStartDate,
          operationStopDate,
          subTaskIndex
        } = taskTableData;

        const formattedStartDate = _getTableDataDateFormattedStringValue(operationStartDate);

        /*
        * Для заданий просто форматируем даты
        * */
        if(subTaskIndex === null)
          return{
            ...taskTableData,
            operationStartDate: formattedStartDate,
            operationStopDate: _getTableDataDateFormattedStringValue(operationStopDate)
          };

        /*
        * Для первых подзаданий дополнительно перезаписываем дату окончания на дату окончания для последнего подзадания
        * этого задания из собранного ранее словаря
        * */

        const lastSubTaskStopDate = lastSubTasksStopDatesMap[_getSubTasksOfOneTaskKey(taskTableData)];

        /*
        * Если нет в словаре, значит последний сабтаск не попал в выборку при запросе тасков на 72 часа, в этом случае
        * выводим в поле дата окончания служебную строку
        * */
        const formattedLastSubTaskStopDate = lastSubTaskStopDate ?
          _getTableDataDateFormattedStringValue(lastSubTaskStopDate) :
          NOT_PLANNED_LABEL;

        return {
          ...taskTableData,
          operationStartDate: formattedStartDate,
          operationStopDate: formattedLastSubTaskStopDate
        }
      })
  }
);

const SIMULATION_OPERATION_TASK_PARAMS_TEMPLATE = {
  operationStartTime: 'startTime',
  operationStartDate: 'startDate',
  operationStopDate: 'stopDate',
  operationId: 'operationId',
  entityBatchId: 'simulationEntityBatchId',
  entityBatchAmountForTask: 'entityAmount',
  subTaskIndex: 'index',
  totalSubTaskAmount: 'of',
  operationType: 'type'
};
const _getSimulationOperationTaskParams = simulationOperationTaskEntity => {
  if(!simulationOperationTaskEntity) return {};

  return{
    ...getEntityDataByTemplate(simulationOperationTaskEntity, SIMULATION_OPERATION_TASK_PARAMS_TEMPLATE),
    operationTypeTitle: TASK_TYPES_TITLES_MAP[simulationOperationTaskEntity.type]
  };
};


const formatOperationDurationByTechnology = formatNumber({integerSeparator: ' ', truncate: 2});

const _getOperationParams = (operationEntity, operationType, entityBatchAmountForTask) => {
  if(!operationEntity) return {};
  const operationTypeDurationByTechnologyField = TASK_TYPES_DURATION_IN_TECHNOLOGY_FIELD_MAP[operationType];

  const {
    identity: operationIdentity,
    nop: operationNumber,
    name: operationName,
    entityRouteId,
    [operationTypeDurationByTechnologyField]: operationDurationByTechnologyInSeconds
  } = operationEntity;

  const operationDurationByTechnologyInHours = operationDurationByTechnologyInSeconds / SECS_IN_HOUR;

  return {
    operationIdentity,
    operationNumber,
    operationName,
    entityRouteId,
    /*
    * Технологическое время всегда указывается в БД на единицу ДСЕ, а задания отображаются на партию.
    * В то же время, время на задания типов наладка или П-З работа не зависит от размера партии и занимает всегда одинаковое
    * время перед началом основной операции над партией.
    * Поэтому для задания типа операций, умножаем технологическое время на количество ДСЕ в партии, чтобы адекватно отобразить
    * длительность, а для задания с типами наладка и П-З работы, просто отображаем длительность, т.к. размер партии не важен
    * */
    operationDurationByTechnologyInHours: formatOperationDurationByTechnology(
      operationType === TASK_TYPES.OPERATION ?
        operationDurationByTechnologyInHours * entityBatchAmountForTask :
        operationDurationByTechnologyInHours
    )
  }
};


const _getEntityRouteParams = entityRouteEntity => {
  if(!entityRouteEntity) return {};

  const {
    identity,
    alternate
  } = entityRouteEntity;
  return{
    entityRouteIdentity: identity,
    isMainEntityRoute: !alternate,
    isMainEntityRouteTitle: alternate ? ALTERNATE_LABEL : MAIN_LABEL
  }
};


const SIMULATION_ENTITY_BATCH_PARAMS_TEMPLATE = {
  entityBatchIdentity: 'identity',
  entityId: 'entityId'
};
const _getSimulationEntityBatchParams = _partialRight(
  getEntityDataByTemplate,
  SIMULATION_ENTITY_BATCH_PARAMS_TEMPLATE
);


const ENTITY_PARAMS_TEMPLATE = {
  entityIdentity: 'identity',
  entityCode: 'code',
  entityName: 'name'
};
const _getEntityParams = _partialRight(
  getEntityDataByTemplate,
  ENTITY_PARAMS_TEMPLATE
);


const _getOrderParams = (simulationOrderEntityBatchEntity, orderEntities) => {
  //если нет simulationOrderEntityBatchEntity, значит это партия без заказа, для такого случая есть служебный
  //параметр WITHOUT_ORDER_LABEL
  if(!simulationOrderEntityBatchEntity){
    return {
      orderId: WITHOUT_ORDER_LABEL,
      orderName: WITHOUT_ORDER_LABEL,
      orderPriority: WITHOUT_ORDER_PRIORITY_LABEL
    }
  }
  const { orderId } = simulationOrderEntityBatchEntity;
  const { name: orderName, priority: orderPriority } = orderEntities[orderId];
  return {
    orderId,
    orderName,
    orderPriority
  }
};

const _getSubTasksOfOneTaskKey = subTaskData => {
  const {
    operationId,
    entityBatchId,
    operationType
  } = subTaskData;
  return [operationId, entityBatchId, operationType].join('_');
};

const _getTableDataDateFormattedStringValue = date => moment(date).format(FORMAT_FULL_TIME);