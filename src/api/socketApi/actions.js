import { createSocketConnection } from './index';

import {fetchSimulationSessionForTasks, logIn} from '../../operations/app';

import { fetchData } from '../httpRequestsApi/actions';
import { sendNotification } from '../../reducers/notification/actions';
import { deleteAllModelEntitiesFromStore } from '../../reducers/entities/actions';
import { clearAllEquipmentClassesInDepartmentData } from '../../reducers/equipmentClassesInDepartment/actions';

import { appStatusSelector } from '../../reducers/appState/selectors';

import { APP_STATUS } from '../../reducers/appState/reducer';
import { NOTIFICATION_LEVEL } from '../../constants/notification';
import {
  SYSTEM_MESSAGES_SOCKET_IDENTITY,
  CLIENT_SOCKET_MESSAGE_TYPE,
  SIMULATION_SESSION_FOR_TASKS_CHANGED_EVENT_TYPE,
  CLIENT_SOCKET_MESSAGE_TEMPORARY_DATA_POINT
} from '../../constants/sockets';
import { DEPARTMENT_MODEL } from '../../constants/models';

import { NEW_TASKS_ARE_READY_MSG } from '../../messages/tasks';

const {
  BACKEND_SERVER_HOST,
  WEBSOCKET_SERVER_HOST
} = window.config;

export const connectToSystemMessagesSocket = () =>
  dispatch => {
    const connectionOptions = {
      onMessageCb: message => dispatch(handleSimulationSessionForTasksChangedEvent(message))
    };
    const socketUrl = [
      WEBSOCKET_SERVER_HOST,
      SYSTEM_MESSAGES_SOCKET_IDENTITY
    ].join('/');
    createSocketConnection(socketUrl, connectionOptions);
  };

export const handleSimulationSessionForTasksChangedEvent = message =>
  (dispatch, getState) => {

    /*
    * Если по каким-то причинам (вроде бы это невозможно) мы оказались подключены к сокетам, но приложение не
    * инициализировано,то не обрабатываем сообщений из сокета
    * */
    const appStatus = appStatusSelector(getState());
    if(appStatus === APP_STATUS.NOT_INITIALIZED) return;

    /*
    * в сокет message приходят много типов сообщений, приложение, здесь обрабатываем только клиентский тип сообщений и
    * только событие о смене сесии для отображаемых заданий
    * */
    if(message.type !== CLIENT_SOCKET_MESSAGE_TYPE) return;

    /*
    * Данные клиентских сообщения могут приходить как в теле самого сообщения, так и храниться на временной точке,
    * с которой их нужно получить
    * */
    const {
      data,
      temporaryDataKey
    } = message;

    const getClientMessageDataAsyncCb = data ?
      () => Promise.resolve(data) :
      () => dispatch(fetchData(
        [
          BACKEND_SERVER_HOST,
          CLIENT_SOCKET_MESSAGE_TEMPORARY_DATA_POINT,
          temporaryDataKey
        ].join('/'),
        {},
        { isBlockingRequest: false }
      ))
        .then(({data}) => data);

    return getClientMessageDataAsyncCb()
      .then(messageDataArr => {
        /*
        * Клиенсткие сообщения могут отправляться с накоплением, поэтому полученные данные сообщения - это всегда массив.
        * Сообщение о смене основной сессии, по идее, должно быть одно, но, теоретически, если быстро поменяли сессии,
        * то может придти и несколько. Определяем есть ли хотя бы одно такое событие и запрашиваем новую сессию,
        * сначала определив новый идентификатор через специальную точку, а потом запросив сессию
        * */

        if(
          messageDataArr.some(({ event }) => event === SIMULATION_SESSION_FOR_TASKS_CHANGED_EVENT_TYPE)
        ){
          /*
          * При принятии новой сессии для заданий, перелогиниваемся, чтобы точно быть в системе при дальнейшем
          * запросе заданий, иначе интерфейс не обновится автоматически. Также, с учетом того, что задания
          * обновляются раз в день то перелогин раз в день позволит постоянно быть сервису ca_frontend быть
          * в системе, что и нужно, и описываемые случаи, когда при определенном запросе истекли сроки будут
          * очень редкими
          * */
          return dispatch(logIn())
            .then(() => {
              dispatch([
                sendNotification(
                  NEW_TASKS_ARE_READY_MSG,
                  NOTIFICATION_LEVEL.SUCCESS,
                  { autoDismiss: 10 }
                ),
                deleteAllModelEntitiesFromStore(DEPARTMENT_MODEL),
                clearAllEquipmentClassesInDepartmentData()
              ]);

              //thunk, поэтому выполняется отдельно
              return dispatch(fetchSimulationSessionForTasks());
            });
        }
      });
  };